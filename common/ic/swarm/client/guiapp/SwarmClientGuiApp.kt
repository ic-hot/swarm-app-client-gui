package ic.swarm.client.guiapp


import ic.stream.output.ByteOutput

import ic.base.reflect.ext.className
import ic.base.throwables.IoException
import ic.base.throwables.UnableToParseException
import ic.base.throwables.WrongTypeException
import ic.base.throwables.WrongValueException
import ic.stream.input.ByteInput
import ic.stream.input.ext.getNextByte
import ic.util.url.Url

import ic.app.gui.GuiApp
import ic.gui.window.Window

import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.link.ext.fromUrlOrThrowUnableToParse
import ic.swarm.fs.type.ContentType.*
import ic.swarm.fs.type.ContentType.File.*

import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow
import ic.swarm.client.guiapp.ui.fs.inithome.InitHomeSwarmDirectoryWindow
import ic.swarm.client.guiapp.ui.fs.png.PngImageWindow
import ic.swarm.client.guiapp.ui.fs.txt.PlainTextWindow
import ic.swarm.client.guiapp.ui.main.MainWindow
import ic.swarm.client.guiapp.ui.service.ServiceWindow
import ic.swarm.client.guiapp.ui.settings.SettingsWindow
import ic.swarm.client.guiapp.ui.windows.WindowType


class SwarmClientGuiApp : GuiApp() {


	@Throws(IoException::class)
	override fun ByteInput.readWindowType() : Window {
		val windowType = getNextByte()
		return when (windowType) {
			WindowType.Main                   -> MainWindow()
			WindowType.Service                -> ServiceWindow()
			WindowType.InitHomeSwarmDirectory -> InitHomeSwarmDirectoryWindow()
			WindowType.Folder                 -> FolderWindow()
			WindowType.PlainText              -> PlainTextWindow()
			WindowType.PngImage              -> PngImageWindow()
			WindowType.Settings               -> SettingsWindow()
			else -> throw WrongValueException.Runtime(
				message = "windowType: $windowType"
			)
		}
	}

	@Throws(UnableToParseException::class)
	override fun parseWindowTypeFromUrlOrThrowUnableToParse (url: Url) : Window {
		val link = SwarmLink.fromUrlOrThrowUnableToParse(url)
		return when (link.contentType) {
			Folder    -> FolderWindow()
			PlainText -> PlainTextWindow()
			PngImage  -> PngImageWindow()
			else -> throw UnableToParseException
		}
	}


	@Throws(IoException::class)
	override fun ByteOutput.writeWindowType (window: Window) {
		putByte(
			when (window) {
				is MainWindow                   -> WindowType.Main
				is ServiceWindow                -> WindowType.Service
				is InitHomeSwarmDirectoryWindow -> WindowType.InitHomeSwarmDirectory
				is FolderWindow                 -> WindowType.Folder
				is PlainTextWindow              -> WindowType.PlainText
				is PngImageWindow               -> WindowType.PngImage
				is SettingsWindow               -> WindowType.Settings
				else -> throw WrongTypeException.Runtime(
					message = "window.className: ${ window.className }"
				)
			}
		)
	}


	override fun ByteOutput.writeInitialWindowTypeAndState (args: String) {
		putByte(WindowType.Main)
	}


}