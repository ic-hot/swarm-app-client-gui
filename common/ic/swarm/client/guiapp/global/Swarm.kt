package ic.swarm.client.guiapp.global


import ic.parallel.funs.doInUiThread

import ic.swarm.Swarm
import ic.swarm.client.guiapp.io.fs.getHomeAccessKey
import ic.swarm.client.guiapp.io.fs.getHomeFolder
import ic.swarm.client.guiapp.ui.broadcast.sendServiceStateChangedBroadcast
import ic.swarm.fs.entry.ext.tryReupload


val swarm = Swarm(

	toUseUpnp = true,

	onStatusChanged = { status ->
		doInUiThread {
			swarmStatus = status
			sendServiceStateChangedBroadcast()
		}
	},

	reuploadImportantData = {
		getHomeFolder()?.tryReupload(
			ownAccessKey = getHomeAccessKey()
		)
	}

)