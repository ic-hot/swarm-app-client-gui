package ic.swarm.client.guiapp.model.addr


import ic.struct.list.List

import ic.swarm.stream.StreamAddress
import ic.swarm.fs.path.AccessKeyAndName


sealed class FsAddress {


	data class NotInHomeFolder (

		val streamAddress : ic.swarm.stream.StreamAddress,

		val access : ic.swarm.access.Access

	) : FsAddress()


	data class InHomeFolder (

		val path : List<AccessKeyAndName>

	) : FsAddress()


}