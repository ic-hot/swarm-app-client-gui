package ic.swarm.client.guiapp.model.addr.funs


import ic.base.primitives.bytes.ext.asInt32
import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.input.ext.getNextByte
import ic.stream.input.ext.readListWithInt32Length
import ic.swarm.access.funs.readAccess

import ic.swarm.stream.funs.readStreamAddress

import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.fs.path.funs.readAccessKeyAndName


@Throws(IoException::class)
fun ByteInput.readFsAddress() : FsAddress {

	val type = getNextByte().asInt32

	when (type) {

		1 -> {
			val streamAddress = readStreamAddress()
			val access = readAccess()
			return FsAddress.NotInHomeFolder(
				streamAddress = streamAddress,
				access = access
			)
		}

		2 -> {
			val path = readListWithInt32Length {
				readAccessKeyAndName()
			}
			return FsAddress.InHomeFolder(
				path = path
			)
		}

		else -> throw RuntimeException("type: $type")

	}

}