package ic.swarm.client.guiapp.model.addr.funs


import ic.base.throwables.IoException
import ic.stream.output.ByteOutput
import ic.stream.output.ext.writeListWithInt32Length
import ic.swarm.access.funs.writeAccess

import ic.swarm.stream.funs.writeStreamAddress

import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.fs.path.funs.writeAccessKeyAndName


@Throws(IoException::class)
fun ByteOutput.writeFsAddress (fsAddress: FsAddress) {

	when (fsAddress) {

		is FsAddress.NotInHomeFolder -> {
			putByte(1)
			writeStreamAddress(fsAddress.streamAddress)
			writeAccess(fsAddress.access)
		}

		is FsAddress.InHomeFolder -> {
			putByte(2)
			writeListWithInt32Length(fsAddress.path) {
				writeAccessKeyAndName(it)
			}
		}

	}

}