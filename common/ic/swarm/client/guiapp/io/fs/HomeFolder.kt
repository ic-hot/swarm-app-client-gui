package ic.swarm.client.guiapp.io.fs


import ic.base.annotations.Blocking
import ic.base.throwables.IoException
import ic.parallel.mutex.Mutex
import ic.parallel.mutex.synchronized
import ic.storage.fs.ext.readFileOrNull
import ic.storage.fs.ext.removeIfExists
import ic.storage.fs.ext.writeFileReplacing
import ic.storage.fs.local.privateFolder

import ic.swarm.access.AccessKey
import ic.swarm.access.funs.readAccessKey
import ic.swarm.access.funs.writeNullableAccessKey
import ic.swarm.client.guiapp.global.swarm
import ic.swarm.ext.fs.createFolder
import ic.swarm.ext.fs.getFolder
import ic.swarm.fs.entry.SwarmFolder
import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.type.ContentType
import ic.swarm.stream.StreamAddress
import ic.swarm.stream.funs.readStreamAddress
import ic.swarm.stream.funs.writeStreamAddress


private val mutex = Mutex()


private data class Saved (
	val streamAddress : StreamAddress,
	val accessKey : AccessKey
)


private var saved : Saved?

	get() = mutex.synchronized {
		return privateFolder.readFileOrNull("homeSwarmFolder") {
			Saved(
				streamAddress = readStreamAddress(),
				accessKey = readAccessKey()
			)
		}
	}

	set (value) = mutex.synchronized {
		if (value == null) {
			privateFolder.removeIfExists("homeSwarmFolder")
		} else {
			privateFolder.writeFileReplacing("homeSwarmFolder") {
				writeStreamAddress(value.streamAddress)
				writeNullableAccessKey(value.accessKey)
			}
		}
	}

;


private var homeSwarmFolder : SwarmFolder? = null


private fun onHomeFolderChanged (homeFolder: SwarmFolder) {
	saved = (saved ?: return).copy(
		streamAddress = homeFolder.streamAddress
	)
}


fun getHomeFolderLink() : SwarmLink? {
	return saved?.let { saved ->
		SwarmLink(
			streamAddress = saved.streamAddress,
			access = saved.accessKey.access,
			contentType = ContentType.Folder,
			name = null
		)
	}
}


fun getHomeFolder() : SwarmFolder? {
	mutex.synchronized {
		return homeSwarmFolder ?: run {
			saved?.let { saved ->
				swarm.getFolder(
					streamAddress = saved.streamAddress,
					access = saved.accessKey.access,
					onChanged = ::onHomeFolderChanged
				)
			}
		}.also { homeSwarmFolder = it }
	}
}


val isHomeFolderSet : Boolean get() = homeSwarmFolder != null || saved != null


fun getHomeAccessKey() : AccessKey? {
	mutex.synchronized {
		return saved?.accessKey
	}
}


fun setHomeFolder (streamAddress: StreamAddress, accessKey: AccessKey) {
	mutex.synchronized {
		saved = Saved(
			streamAddress = streamAddress,
			accessKey = accessKey
		)
		homeSwarmFolder = null
	}
}


@Blocking
@Throws(IoException::class)
fun setHomeFolderToEmpty (accessKey: AccessKey) {
	mutex.synchronized {
		val folder = swarm.createFolder(
			ownAccessKey = accessKey,
			thumbnail = null,
			onChanged = ::onHomeFolderChanged
		)
		saved = Saved(
			streamAddress = folder.streamAddress,
			accessKey = accessKey
		)
		homeSwarmFolder = null
	}
}