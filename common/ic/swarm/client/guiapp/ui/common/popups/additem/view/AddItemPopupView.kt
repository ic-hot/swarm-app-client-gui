package ic.swarm.client.guiapp.ui.common.popups.additem.view


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.text.Text
import ic.gui.scope.ext.views.touch.screening.TouchScreening

import ic.res.icons.material.MaterialIcon

import ic.swarm.client.guiapp.ui.common.popups.additem.AddItemPopup
import ic.swarm.client.guiapp.ui.common.popups.additem.impl.pasteLink
import ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.showNameFilePopup
import ic.swarm.client.guiapp.ui.common.theme.theme


@Suppress("FunctionName")
internal fun AddItemPopup.AddItemPopupView() = (

	Material(
		cornersRadius = 8.dp,
		elevation = 16.dp,
		child = Stack(
			width = 256.dp, height = ByContent,
			children = List(
				Solid(color = Color.White),
				TouchScreening(),
				Padding(
					padding = 8.dp,
					child = Column(
						width = ByContainer, height = ByContent,
						children = List(
							Text(
								width = ByContainer,
								size = 16.dp,
								value = "Add item:",
								color = Color.Black
							),
							Space(height = 8.dp),
							Item(
								icon = MaterialIcon.Folder,
								color = theme.folderColor,
								text = "Folder",
								onClick = {
									showNameFilePopup(
										accentColor = theme.folderColor,
										hintText = "Folder name",
										onCancel = {},
										onDone = { name, accessKey ->
											closePopup()
											createFolder(name, accessKey)
										}
									)
								}
							),
							Space(height = 4.dp),
							Item(
								icon = MaterialIcon.FileDocumentOutline,
								color = theme.plainTextColor,
								text = "Plain text",
								onClick = {
									showNameFilePopup(
										accentColor = theme.plainTextColor,
										hintText = "File name",
										onCancel = {},
										onDone = { name, accessKey ->
											createPlainText(name, accessKey)
											closePopup()
										}
									)
								}
							),
							Space(height = 4.dp),
							Item(
								icon = MaterialIcon.FileImageOutline,
								color = theme.imageColor,
								text = "Image",
								onClick = {
									showNameFilePopup(
										accentColor = theme.imageColor,
										hintText = "File name",
										onCancel = {},
										onDone = { name, accessKey ->
											createImage(name, accessKey)
											closePopup()
										}
									)
								}
							),
							Space(height = 4.dp),
							Item(
								icon = MaterialIcon.Link,
								color = theme.neutralColor,
								text = "Paste link",
								onClick = ::pasteLink
							)
						)
					)
				)
			)
		)
	)

)