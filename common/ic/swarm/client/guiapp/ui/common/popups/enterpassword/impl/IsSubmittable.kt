package ic.swarm.client.guiapp.ui.common.popups.enterpassword.impl


import ic.swarm.client.guiapp.ui.common.popups.enterpassword.EnterPasswordPopup


internal val EnterPasswordPopup.isSubmittable get() = isPasswordValid