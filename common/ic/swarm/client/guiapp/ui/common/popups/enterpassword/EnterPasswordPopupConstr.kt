package ic.swarm.client.guiapp.ui.common.popups.enterpassword


inline fun EnterPasswordPopup (

	crossinline onCloseWithoutResult : () -> Unit,

	crossinline onCloseWithResult : (password: String) -> Unit

) : EnterPasswordPopup {

	return object : EnterPasswordPopup() {

		override fun onCloseWithoutResult() {
			onCloseWithoutResult()
		}

		override fun onCloseWithResult (password: String) {
			onCloseWithResult(password)
		}

	}

}