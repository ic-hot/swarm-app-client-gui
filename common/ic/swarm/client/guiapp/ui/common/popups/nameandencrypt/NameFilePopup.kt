package ic.swarm.client.guiapp.ui.common.popups.nameandencrypt


import ic.graphics.color.Color

import ic.gui.control.ext.DecorPadding
import ic.gui.popup.Popup

import ic.swarm.access.AccessKey

import ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.view.NameFilePopupView


internal abstract class NameFilePopup (

	internal val accentColor : Color,

	internal val hintText : String

) : Popup() {


	abstract fun onCancel()

	abstract fun onDone (fileName: String, ownAccessKey: ic.swarm.access.AccessKey?)


	var ownAccessKey : ic.swarm.access.AccessKey? = null

	var fileName : String = ""


	override fun initView() = DecorPadding(
		child = NameFilePopupView()
	)


	private var isDone : Boolean = false

	override fun onClosePopup (byUser: Boolean) {
		if (isDone) {
			onDone(fileName, ownAccessKey)
		} else {
			onCancel()
		}
	}

	fun notifyDone() {
		isDone = true
		closePopup()
	}


	override fun onClose() {
		hideKeyboard()
		ownAccessKey = null
		fileName = ""
		isDone = false
	}


}