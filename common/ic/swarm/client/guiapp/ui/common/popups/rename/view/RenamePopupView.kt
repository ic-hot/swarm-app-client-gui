package ic.swarm.client.guiapp.ui.common.popups.rename.view


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.control.ext.TopDecorSpace
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.touch.screening.TouchScreening
import ic.gui.view.textinput.OnOkayAction.Companion.Done

import ic.swarm.client.guiapp.ui.common.popups.rename.RenamePopup
import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.common.views.MaterialTextInput


@Suppress("FunctionName")
internal fun RenamePopup.RenamePopupView() = (

	Column(
		width = ByContainer, height = ByContainer,
		children = List(
			TopDecorSpace(),
			Padding(
				padding = 16.dp,
				child = Material(
					cornersRadius = 8.dp,
					elevation = 16.dp,
					child = Stack(
						width = ByContainer, height = ByContent,
						children = List(
							Solid(color = Color.White),
							TouchScreening(),
							Padding(
								padding = 8.dp,
								child = Column(
									width = ByContainer, height = ByContent,
									children = List(
										MaterialTextInput(
											getOutlineColor = { theme.neutralColor },
											hintText = "New name",
											toRequestKeyboard = true,
											getValue = { name },
											onValueChanged = {
												name = it
												updateView()
											},
											onOkayAction = Done {
												notifyDone()
											}
										)
									)
								)
							)
						)
					)
				)
			),
			Space()
		)
	)

)