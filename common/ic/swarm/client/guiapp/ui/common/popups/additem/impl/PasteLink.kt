package ic.swarm.client.guiapp.ui.common.popups.additem.impl


import ic.base.throwables.UnableToParseException

import ic.swarm.client.guiapp.ui.common.popups.additem.AddItemPopup
import ic.swarm.client.guiapp.ui.common.popups.rename.showRenamePopup
import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.link.ext.fromUrlOrThrowUnableToParse


internal fun AddItemPopup.pasteLink() {

	val string = getStringFromClipboard()
	if (string == null) {
		showToast("Clipboard is empty")
	} else {
		val link = (
			try {
				SwarmLink.fromUrlOrThrowUnableToParse(string)
			} catch (_: UnableToParseException) {
				showToast("String in clipboard is not a Swarm link")
				return
			}
		)
		showRenamePopup(
			onCancel = {},
			onDone = { newName ->
				pasteLink(newName, link)
				closePopup()
			}
		)
	}

}