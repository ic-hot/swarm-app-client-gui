package ic.swarm.client.guiapp.ui.common.popups.itemmenu


import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope

import ic.swarm.fs.entry.SwarmFsEntry


internal fun GuiScope.showItemMenuPopup (

	item : SwarmFsEntry,

	isEditable : Boolean,

	removeItem : () -> Unit,

	renameItem : (newName: String) -> Unit

) {
	showPopup(
		isCloseable = true,
		backgroundBlurRadius = 16.dp,
		popup = ItemMenuPopup(
			entry = item,
			isEditable = isEditable,
			removeItem = removeItem,
			renameItem = renameItem
		)
	)
}