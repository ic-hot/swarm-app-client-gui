package ic.swarm.client.guiapp.ui.common.popups.additem.view


import ic.struct.list.List

import ic.graphics.color.Color
import ic.graphics.color.Color.Companion.Black
import ic.graphics.color.Color.Companion.White
import ic.graphics.image.Image

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.clickable.container.Clickable
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.text.Text
import ic.swarm.client.guiapp.ui.common.views.Icon


@Suppress("FunctionName")
fun GuiScope.Item (

	icon : Image,
	color : Color,
	text : String,

	onClick : () -> Unit

) = (

	Clickable(
		onClick = onClick,
		child = Material(
			elevation = 4.dp,
			child = Stack(
				width = ByContainer, height = 48.dp,
				children = List(
					Solid(color = White),
					Row(
						width = ByContainer, height = ByContainer,
						children = List(
							Icon(
								icon = icon,
								color = color
							),
							Text(
								width = ByContainer,
								size = 14.dp,
								value = text,
								color = Black
							),
							Space(width = 12.dp)
						)
					)
				)
			)
		)
	)

)