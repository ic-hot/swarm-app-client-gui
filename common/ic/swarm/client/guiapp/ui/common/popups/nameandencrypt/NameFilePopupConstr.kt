package ic.swarm.client.guiapp.ui.common.popups.nameandencrypt


import ic.graphics.color.Color

import ic.swarm.access.AccessKey


internal inline fun NameFilePopup (

	accentColor : Color,

	hintText : String,

	crossinline onCancel : () -> Unit,

	crossinline onDone : (fileName: String, ownAccessKey: ic.swarm.access.AccessKey?) -> Unit

) : NameFilePopup {

	return object : NameFilePopup(
		accentColor = accentColor,
		hintText = hintText
	) {

		override fun onCancel() {
			onCancel()
		}

		override fun onDone (fileName: String, ownAccessKey: ic.swarm.access.AccessKey?) {
			onDone(fileName, ownAccessKey)
		}

	}

}