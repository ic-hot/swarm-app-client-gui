package ic.swarm.client.guiapp.ui.common.popups.nameandencrypt


import ic.graphics.color.Color

import ic.gui.align.Top
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope

import ic.swarm.access.AccessKey


internal inline fun GuiScope.showNameFilePopup (

	accentColor : Color,

	hintText : String,

	crossinline onCancel : () -> Unit,

	crossinline onDone : (fileName: String, ownAccessKey: ic.swarm.access.AccessKey?) -> Unit

) {

	showPopup(
		isCloseable = true,
		verticalAlign = Top,
		backgroundBlurRadius = 16.dp,
		popup = NameFilePopup(
			accentColor = accentColor,
			hintText = hintText,
			onCancel = onCancel,
			onDone = onDone
		)
	)

}