package ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.view


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.text.Text

import ic.res.icons.material.MaterialIcon

import ic.swarm.access.Access
import ic.swarm.access.AccessKey

import ic.swarm.client.guiapp.ui.common.popups.createpassword.showCreatePasswordPopup
import ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.NameFilePopup
import ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.impl.ownAccess
import ic.swarm.client.guiapp.ui.common.views.select.SelectableIcon


@Suppress("FunctionName")
internal fun NameFilePopup.EncryptionBlock() = (

	Column(
		width = ByContainer, height = ByContent,
		children = List(

			Row(
				width = ByContainer, height = ByContent,
				children = List(

					Text(
						width = ByContainer,
						size = 16.dp,
						value = "Encryption: ",
						color = Color.Black
					),

					Material(
						cornersRadius = 8.dp,
						outlineThickness = 2.dp,
						outlineColor = Color.Gray,
						child = Row(
							width = ByContent, height = ByContent,
							children = List(
								SelectableIcon(
									icon = MaterialIcon.LockOpenOutline,
									isSelected = { ownAccess == ic.swarm.access.Access.Everyone },
									onClick = {
										ownAccessKey = ic.swarm.access.AccessKey.None
										updateView()
									}
								),
								SelectableIcon(
									icon = MaterialIcon.LockOutline,
									isSelected = {
										ownAccess == ic.swarm.access.Access.Restricted.WhoeverHasPassword
									},
									onClick = {
										showCreatePasswordPopup(
											onCloseWithResult = { password ->
												ownAccessKey = ic.swarm.access.AccessKey.Private.Password(
													password
												)
												updateView()
											}
										)
									}
								)
							)
						)
					)

				)
			)

		)
	)

)