package ic.swarm.client.guiapp.ui.common.popups.enterpassword


import ic.gui.align.Top
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope


fun GuiScope.showEnterPasswordPopup (

	onCloseWithoutResult : () -> Unit,

	onCloseWithResult : (String) -> Unit

) {

	showPopup(
		isCloseable = true,
		verticalAlign = Top,
		backgroundBlurRadius = 16.dp,
		popup = EnterPasswordPopup(
			onCloseWithoutResult = onCloseWithoutResult,
			onCloseWithResult = onCloseWithResult
		)
	)

}