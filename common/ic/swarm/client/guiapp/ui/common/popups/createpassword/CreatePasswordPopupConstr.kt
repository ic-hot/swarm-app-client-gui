package ic.swarm.client.guiapp.ui.common.popups.createpassword


inline fun CreatePasswordPopup (

	crossinline onCloseWithoutResult : () -> Unit,

	crossinline onCloseWithResult : (password: String) -> Unit

) : CreatePasswordPopup {

	return object : CreatePasswordPopup() {

		override fun onCloseWithoutResult() {
			onCloseWithoutResult()
		}

		override fun onCloseWithResult (password: String) {
			onCloseWithResult(password)
		}

	}

}