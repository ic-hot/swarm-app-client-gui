package ic.swarm.client.guiapp.ui.common.popups.createpassword


import ic.gui.popup.Popup

import ic.swarm.client.guiapp.ui.common.popups.createpassword.view.CreatePasswordPopupView


abstract class CreatePasswordPopup : Popup() {


	protected abstract fun onCloseWithoutResult()

	protected abstract fun onCloseWithResult (password: String)

	private var result : String? = null

	internal fun closeWithResult (password: String) {
		result = password
		closePopup()
	}

	override fun onClosePopup (byUser: Boolean) {
		hideKeyboard()
		val result = this.result.also { this.result = null }
		if (result == null) {
			onCloseWithoutResult()
		} else {
			onCloseWithResult(result)
		}
	}


	internal var password1 : String = ""

	internal var password2 : String = ""


	override fun initView() = CreatePasswordPopupView()


}