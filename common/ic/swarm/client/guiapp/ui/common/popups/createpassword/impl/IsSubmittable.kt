package ic.swarm.client.guiapp.ui.common.popups.createpassword.impl


import ic.swarm.client.guiapp.ui.common.popups.createpassword.CreatePasswordPopup


internal val CreatePasswordPopup.isSubmittable get() = isPassword1Valid && isPassword2Valid