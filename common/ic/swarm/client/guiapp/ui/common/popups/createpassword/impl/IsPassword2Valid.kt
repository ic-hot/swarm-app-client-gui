package ic.swarm.client.guiapp.ui.common.popups.createpassword.impl


import ic.swarm.client.guiapp.ui.common.popups.createpassword.CreatePasswordPopup


internal val CreatePasswordPopup.isPassword2Valid get() = password2 == password1