package ic.swarm.client.guiapp.ui.common.popups.createpassword.view


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.control.ext.TopDecorSpace
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.text.Text
import ic.gui.scope.ext.views.touch.screening.TouchScreening

import ic.swarm.client.guiapp.ui.common.popups.createpassword.CreatePasswordPopup
import ic.swarm.client.guiapp.ui.common.popups.createpassword.impl.isPassword1Valid
import ic.swarm.client.guiapp.ui.common.popups.createpassword.impl.isPassword2Valid
import ic.swarm.client.guiapp.ui.common.popups.createpassword.impl.isSubmittable
import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.common.views.MaterialTextInput
import ic.swarm.client.guiapp.ui.common.views.buttons.ButtonState
import ic.swarm.client.guiapp.ui.common.views.buttons.Button


@Suppress("FunctionName")
internal fun CreatePasswordPopup.CreatePasswordPopupView() = (

	Column(
		width = ByContainer, height = ByContainer,
		children = List(

			TopDecorSpace(),

			Space(height = 16.dp),

			Material(
				cornersRadius = 16.dp,
				elevation = 16.dp,
				child = Stack(
					width = 256.dp, height = ByContent,
					children = List(
						Solid(color = Color.White),
						TouchScreening(),
						Padding(
							padding = 16.dp,
							child = Column(
								width = ByContainer, height = ByContent,
								children = List(
									Text(
										width = ByContainer,
										size = 16.dp,
										value = "Create password",
										color = Color.Black
									),
									Space(height = 16.dp),
									MaterialTextInput(
										getOutlineColor = {
											if (isPassword1Valid) {
												theme.okayColor
											} else {
												theme.errorColor
											}
										},
										hintText = "New password",
										toRequestKeyboard = true,
										getValue = { password1 },
										onValueChanged = { password1 = it; updateView() }
									),
									Space(height = 8.dp),
									MaterialTextInput(
										getOutlineColor = {
											if (isPassword2Valid) {
												theme.okayColor
											} else {
												theme.errorColor
											}
										},
										hintText = "New password again",
										getValue = { password2 },
										onValueChanged = { password2 = it; updateView() }
									),
									Space(height = 16.dp),
									Button(
										text = "Done",
										getState = {
											if (isSubmittable) {
												ButtonState.Active
											} else {
												ButtonState.Inactive
											}
										},
										onClick = {
											closeWithResult(password1)
										}
									)
								)
							)
						)
					)
				)
			),

			Space()

		)
	)

)