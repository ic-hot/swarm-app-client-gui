package ic.swarm.client.guiapp.ui.common.popups.itemmenu.view


import ic.base.escape.skippable.skip
import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.touch.screening.TouchScreening

import ic.swarm.client.guiapp.ui.common.popups.itemmenu.ItemMenuPopup
import ic.swarm.client.guiapp.ui.common.popups.rename.showRenamePopup
import ic.swarm.client.guiapp.ui.common.views.buttons.Button
import ic.swarm.fs.entry.ext.link


@Suppress("FunctionName")
internal fun ItemMenuPopup.ItemMenuPopupView() = (

	Material(
		cornersRadius = 16.dp,
		elevation = 16.dp,
		child = Stack(
			width = 256.dp, height = ByContent,
			children = List(
				Solid(color = Color.White),
				TouchScreening(),
				Padding(
					padding = 8.dp,
					child = Column(
						width = ByContainer, height = ByContent,
						children = List(

							{
								Button(
									text = "Copy link",
									onClick = {
										copyToClipboard(
											entry.link.toString()
										)
										showToast("Copied to clipboard")
										closePopup()
									}
								)
							},

							{ Space(height = 8.dp) },

							{
								if (!isEditable) skip
								Button(
									text = "Remove",
									onClick = {
										removeItem()
										closePopup()
									}
								)
							},

							{ Space(height = 8.dp) },

							{
								if (!isEditable) skip
								Button(
									text = "Rename",
									onClick = {
										showRenamePopup(
											onCancel = {},
											onDone = { newName ->
												renameItem(newName)
												closePopup()
											}
										)
									}
								)
							}

						)
					)
				)
			)
		)
	)

)