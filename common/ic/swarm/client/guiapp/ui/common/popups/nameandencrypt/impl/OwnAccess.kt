package ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.impl


import ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.NameFilePopup


internal val NameFilePopup.ownAccess get() = ownAccessKey?.access