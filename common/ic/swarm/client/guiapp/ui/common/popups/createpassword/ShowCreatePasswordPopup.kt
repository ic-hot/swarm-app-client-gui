package ic.swarm.client.guiapp.ui.common.popups.createpassword


import ic.gui.align.Top
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope


fun GuiScope.showCreatePasswordPopup (

	onCloseWithoutResult : () -> Unit = {},

	onCloseWithResult : (String) -> Unit

) {

	showPopup(
		isCloseable = true,
		verticalAlign = Top,
		backgroundBlurRadius = 16.dp,
		popup = CreatePasswordPopup(
			onCloseWithoutResult = onCloseWithoutResult,
			onCloseWithResult = onCloseWithResult
		)
	)

}