package ic.swarm.client.guiapp.ui.common.popups.additem


import ic.swarm.access.AccessKey
import ic.swarm.fs.link.SwarmLink


inline fun AddItemPopup (

	crossinline createFolder : (name: String, ownAccessKey: AccessKey?) -> Unit,

	crossinline createImage : (name: String, ownAccessKey: AccessKey?) -> Unit,

	crossinline createPlainText : (name: String, ownAccessKey: AccessKey?) -> Unit,

	crossinline pasteLink : (name: String, SwarmLink) -> Unit

) : AddItemPopup {

	return object : AddItemPopup() {

		override fun createFolder (name: String, ownAccessKey: AccessKey?) {
			createFolder(name, ownAccessKey)
		}

		override fun createImage (name: String, ownAccessKey: AccessKey?) {
			createImage(name, ownAccessKey)
		}

		override fun createPlainText (name: String, ownAccessKey: AccessKey?) {
			createPlainText(name, ownAccessKey)
		}

		override fun pasteLink (name: String, link: SwarmLink) {
			pasteLink(name, link)
		}

	}

}