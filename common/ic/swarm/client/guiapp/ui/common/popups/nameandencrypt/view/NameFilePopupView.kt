package ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.view


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.align.Top
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.touch.screening.TouchScreening
import ic.gui.view.textinput.OnOkayAction.Companion.Done

import ic.swarm.client.guiapp.ui.common.popups.nameandencrypt.NameFilePopup
import ic.swarm.client.guiapp.ui.common.views.MaterialTextInput


@Suppress("FunctionName")
internal fun NameFilePopup.NameFilePopupView() = (

	Padding(
		verticalAlign = Top,
		padding = 16.dp,
		child = Material(
			cornersRadius = 8.dp,
			elevation = 16.dp,
			child = Stack(
				width = ByContainer, height = ByContent,
				children = List(
					Solid(color = Color.White),
					TouchScreening(),
					Padding(
						padding = 8.dp,
						child = Column(
							width = ByContainer, height = ByContent,
							children = List(
								EncryptionBlock(),
								Space(height = 8.dp),
								MaterialTextInput(
									getOutlineColor = { accentColor },
									hintText = hintText,
									toRequestKeyboard = true,
									getValue = { fileName },
									onValueChanged = { fileName = it },
									onOkayAction = Done {
										notifyDone()
									}
								)
							)
						)
					)
				)
			)
		)
	)

)