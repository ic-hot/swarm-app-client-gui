package ic.swarm.client.guiapp.ui.common.popups.rename


import ic.gui.popup.Popup

import ic.swarm.client.guiapp.ui.common.popups.rename.view.RenamePopupView


internal abstract class RenamePopup : Popup() {


	protected abstract fun onCancel()

	protected abstract fun onDone (newName: String)


	var name : String = ""


	override fun initView() = RenamePopupView()


	private var isDone : Boolean = false

	fun notifyDone() {
		isDone = true
		closePopup()
	}

	override fun onClosePopup (byUser: Boolean) {
		if (isDone) {
			onDone(name)
		} else {
			onCancel()
		}
	}


	override fun onClose() {
		hideKeyboard()
		name = ""
		isDone = false
	}


}