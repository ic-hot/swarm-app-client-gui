package ic.swarm.client.guiapp.ui.common.popups.additem


import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope

import ic.swarm.access.AccessKey
import ic.swarm.fs.link.SwarmLink


inline fun GuiScope.showAddItemPopup (

	crossinline createFolder    : (name: String, ownAccessKey: AccessKey?) -> Unit,
	crossinline createImage     : (name: String, ownAccessKey: AccessKey?) -> Unit,
	crossinline createPlainText : (name: String, ownAccessKey: AccessKey?) -> Unit,

	crossinline pasteLink : (name: String, link: SwarmLink) -> Unit

) {
	showPopup(
		isCloseable = true,
		backgroundBlurRadius = 16.dp,
		popup = AddItemPopup(
			createFolder = createFolder,
			createImage = createImage,
			createPlainText = createPlainText,
			pasteLink = pasteLink
		)
	)
}