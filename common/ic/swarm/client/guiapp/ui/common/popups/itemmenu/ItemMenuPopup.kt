package ic.swarm.client.guiapp.ui.common.popups.itemmenu


import ic.gui.popup.Popup

import ic.swarm.client.guiapp.ui.common.popups.itemmenu.view.ItemMenuPopupView
import ic.swarm.fs.entry.SwarmFsEntry


internal abstract class ItemMenuPopup (

	val entry : SwarmFsEntry,

	val isEditable : Boolean

) : Popup() {

	abstract fun removeItem()

	abstract fun renameItem (newName: String)

	override fun initView() = ItemMenuPopupView()

}