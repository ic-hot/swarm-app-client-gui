package ic.swarm.client.guiapp.ui.common.popups.enterpassword


import ic.gui.popup.Popup

import ic.swarm.client.guiapp.ui.common.popups.enterpassword.view.EnterPasswordPopupView


abstract class EnterPasswordPopup : Popup() {


	protected abstract fun onCloseWithoutResult()

	protected abstract fun onCloseWithResult (password: String)


	private var result : String? = null


	internal var password : String = ""


	override fun initView() = EnterPasswordPopupView()


	internal fun closeWithResult (password: String) {
		result = password
		closePopup()
	}

	override fun onClosePopup (byUser: Boolean) {
		hideKeyboard()
		val result = this.result
		if (result == null) {
			onCloseWithoutResult()
		} else {
			onCloseWithResult(result)
		}
	}


}