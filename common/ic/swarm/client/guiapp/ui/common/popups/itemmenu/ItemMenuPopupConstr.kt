package ic.swarm.client.guiapp.ui.common.popups.itemmenu


import ic.swarm.fs.entry.SwarmFsEntry


internal inline fun ItemMenuPopup (

	entry : SwarmFsEntry,

	isEditable : Boolean,

	crossinline removeItem : () -> Unit,

	crossinline renameItem : (newName: String) -> Unit

) : ItemMenuPopup {

	return object : ItemMenuPopup(
		entry = entry,
		isEditable = isEditable
	) {

		override fun removeItem() = removeItem()

		override fun renameItem (newName: String) {
			renameItem(newName)
		}

	}

}