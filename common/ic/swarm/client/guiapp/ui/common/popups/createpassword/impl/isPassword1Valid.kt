package ic.swarm.client.guiapp.ui.common.popups.createpassword.impl


import ic.swarm.client.guiapp.ui.common.popups.createpassword.CreatePasswordPopup


internal val CreatePasswordPopup.isPassword1Valid get() = password1.length > 3