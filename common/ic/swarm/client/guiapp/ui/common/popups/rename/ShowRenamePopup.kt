package ic.swarm.client.guiapp.ui.common.popups.rename


import ic.gui.align.Top
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope


internal inline fun GuiScope.showRenamePopup (

	crossinline onCancel : () -> Unit,

	crossinline onDone : (newName: String) -> Unit

) {

	showPopup(
		isCloseable = true,
		verticalAlign = Top,
		backgroundBlurRadius = 16.dp,
		popup = RenamePopup(
			onCancel = onCancel,
			onDone = onDone
		)
	)

}