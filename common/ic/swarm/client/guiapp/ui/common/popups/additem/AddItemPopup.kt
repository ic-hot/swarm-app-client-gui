package ic.swarm.client.guiapp.ui.common.popups.additem


import ic.gui.popup.Popup

import ic.swarm.access.AccessKey
import ic.swarm.fs.link.SwarmLink

import ic.swarm.client.guiapp.ui.common.popups.additem.view.AddItemPopupView


abstract class AddItemPopup : Popup() {


	abstract fun createFolder (name: String, ownAccessKey: AccessKey?)

	abstract fun createImage (name: String, ownAccessKey: AccessKey?)

	abstract fun createPlainText (name: String, ownAccessKey: AccessKey?)

	abstract fun pasteLink (name: String, link: SwarmLink)


	override fun initView() = AddItemPopupView()


}