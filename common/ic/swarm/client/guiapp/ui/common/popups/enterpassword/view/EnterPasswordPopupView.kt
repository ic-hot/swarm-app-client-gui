package ic.swarm.client.guiapp.ui.common.popups.enterpassword.view


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.control.ext.TopDecorSpace
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.touch.screening.TouchScreening
import ic.gui.view.textinput.OnOkayAction

import ic.swarm.client.guiapp.ui.common.popups.enterpassword.EnterPasswordPopup
import ic.swarm.client.guiapp.ui.common.popups.enterpassword.impl.isPasswordValid
import ic.swarm.client.guiapp.ui.common.popups.enterpassword.impl.isSubmittable
import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.common.views.MaterialTextInput


@Suppress("FunctionName")
internal fun EnterPasswordPopup.EnterPasswordPopupView() = (

	Column(
		width = ByContainer, height = ByContainer,
		children = List(

			TopDecorSpace(),

			Space(height = 16.dp),

			Material(
				cornersRadius = 8.dp,
				elevation = 16.dp,
				child = Stack(
					width = 256.dp, height = ByContent,
					children = List(
						Solid(color = Color.White),
						TouchScreening(),
						Padding(
							padding = 8.dp,
							child = Column(
								width = ByContainer, height = ByContent,
								children = List(
									MaterialTextInput(
										getOutlineColor = {
											if (isPasswordValid) {
												theme.okayColor
											} else {
												theme.errorColor
											}
										},
										hintText = "Password",
										toRequestKeyboard = true,
										getValue = { password },
										onValueChanged = { password = it; updateView() },
										onOkayAction = OnOkayAction.Done {
											if (isSubmittable) {
												closeWithResult(password)
											}
										}
									)
								)
							)
						)
					)
				)
			)

		)
	)

)