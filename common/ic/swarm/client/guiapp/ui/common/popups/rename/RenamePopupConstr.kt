package ic.swarm.client.guiapp.ui.common.popups.rename


internal inline fun RenamePopup (

	crossinline onCancel : () -> Unit,

	crossinline onDone : (newName: String) -> Unit

) : RenamePopup {

	return object : RenamePopup() {

		override fun onCancel() = onCancel()

		override fun onDone (newName: String) = onDone(name)

	}

}