package ic.swarm.client.guiapp.ui.common.theme


import ic.graphics.color.Color
import ic.graphics.color.Color.Companion.Gray
import ic.graphics.color.Color.Companion.EvenLighterGray


object DefaultTheme : Theme {

	override val backgroundColor get() = EvenLighterGray

	override val neutralColor get() = Gray

	override val okayColor = Color(0.25, .1, 0.25)

	override val errorColor = Color(1.0, 0.0, 0.0)

	override val folderColor = Color(1.0, .75, .5)

	override val plainTextColor = Color(0.5, .75, 1.0)

	override val imageColor = Color(0.5, .75, 0.5)

}