package ic.swarm.client.guiapp.ui.common.theme


import ic.graphics.color.Color


interface Theme {

	val backgroundColor : Color

	val neutralColor : Color

	val okayColor : Color

	val errorColor : Color

	val folderColor : Color

	val plainTextColor : Color

	val imageColor : Color

}