package ic.swarm.client.guiapp.ui.common


import ic.swarm.status.SwarmStatus.*

import ic.swarm.client.guiapp.global.isServiceShouldBeRunning
import ic.swarm.client.guiapp.global.swarmStatus


val serviceStateMessage : String get() = (

	if (isServiceShouldBeRunning) {

		when (swarmStatus) {

			Starting -> "Node is starting..."

			is Running -> "Node is running"

			Stopping -> "Node is stopping due to connection failure..."

			Idle -> "Waiting for connection to establish"

		}

	} else {

		"Node is not running"

	}

)