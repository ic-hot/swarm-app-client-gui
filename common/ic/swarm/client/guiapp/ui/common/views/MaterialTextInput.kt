package ic.swarm.client.guiapp.ui.common.views


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.textinput.TextInput
import ic.gui.view.textinput.OnOkayAction


@Suppress("FunctionName")
fun GuiScope.MaterialTextInput (

	getOutlineColor : () -> Color,

	hintText : String,

	toRequestKeyboard : Boolean = false,

	getValue : () -> String,

	onValueChanged : (String) -> Unit,

	onOkayAction : OnOkayAction = OnOkayAction.HideKeyboard

) = (

	Material(
		cornersRadius = 4.dp,
		outlineThickness = 2.dp,
		getOutlineColor = getOutlineColor,
		child = Row(
			width = ByContainer, height = 48.dp,
			children = List(
				Padding(
					horizontal = 8.dp,
					child = TextInput(
						width = ByContainer, height = ByContent,
						size = 16.dp,
						maxLinesCount = 1,
						hintText = hintText,
						getValue = getValue,
						onValueChanged = onValueChanged,
						color = Color.Black,
						hintColor = Color.Gray,
						toRequestKeyboard = toRequestKeyboard,
						onOkayAction = onOkayAction
					)
				)
			)
		)
	)

)