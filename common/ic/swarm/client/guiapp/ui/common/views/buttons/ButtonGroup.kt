package ic.swarm.client.guiapp.ui.common.views.buttons


import ic.struct.list.List
import ic.struct.list.ext.copy.copy

import ic.graphics.color.Color

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View

import ic.swarm.client.guiapp.ui.common.theme.theme


@Suppress("FunctionName")
fun GuiScope.ButtonGroup (

	backgroundColor : Color = Color.White,

	children : List<out View>

) = (

	Material(
		cornersRadius = 8.dp,
		elevation = 4.dp,
		child = Stack(
			width = ByContainer, height = ByContent,
			children = List(
				Solid(color = backgroundColor),
				Column(
					width = ByContainer, height = ByContent,
					children = children.copy(
						initItemInBetween = {
							Solid(
								height = 1.dp,
								color = theme.neutralColor
							)
						}
					)
				)
			)
		)
	)

)