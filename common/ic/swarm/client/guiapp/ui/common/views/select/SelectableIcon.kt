package ic.swarm.client.guiapp.ui.common.views.select


import ic.struct.list.List

import ic.graphics.color.Color.Companion.Black
import ic.graphics.image.Image

import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.clickable.container.Clickable
import ic.gui.scope.ext.views.image.Image
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.image.ScaleMode


@Suppress("FunctionName")
fun GuiScope.SelectableIcon (

	icon : Image,

	isSelected : () -> Boolean,

	onClick : () -> Unit

) = (

	Clickable(
		onClick = onClick,
		child = Stack(
			width = 48.dp, height = 48.dp,
			children = List(
				Image(
					width = 24.dp, height = 24.dp,
					scaleMode = ScaleMode.Fit,
					image = icon,
					tint = Black,
					getOpacity = {
						if (isSelected()) 1F else .5F
					}
				)
			)
		)
	)

)