package ic.swarm.client.guiapp.ui.common.views.decor


import ic.struct.list.List
import ic.struct.list.ext.plus

import ic.graphics.color.Color
import ic.graphics.color.Color.Companion.Black

import ic.gui.align.Top
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.text.Text
import ic.gui.view.View
import ic.gui.control.ext.TopDecorSpace
import ic.gui.window.Window


@Suppress("FunctionName")
internal fun Window.TopBar(

	color : Color,

	titleText : String,

	rightViews : List<out View> = List()

) = (

	Material(
		elevation = 8.dp,
		verticalAlign = Top,
		child = Stack(
			width = ByContainer, height = ByContent,
			children = List(
				Solid(color = color),
				Column(
					width = ByContainer, height = ByContent,
					children = List(
						TopDecorSpace(),
						Row(
							width = ByContainer, height = 48.dp,
							children = List(
								Space(width = 16.dp),
								Text(
									width = ByContainer,
									ellipsize = true,
									size = 14.dp,
									value = titleText,
									color = Black
								)
							) + rightViews
						)
					)
				)
			)
		)
	)

)