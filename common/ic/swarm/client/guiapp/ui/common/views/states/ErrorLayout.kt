package ic.swarm.client.guiapp.ui.common.views.states


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.align.Center
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.text.Text
import ic.gui.window.Window


@Suppress("FunctionName")
internal fun Window.ErrorLayout (

	text : String

) = (

	Stack(
		width = ByContainer, height = ByContainer,
		children = List(
			Padding(
				horizontal = 48.dp,
				child = Text(
					width = ByContainer,
					textHorizontalAlign = Center,
					size = 16.dp,
					value = text,
					color = Color.Gray
				)
			)
		)
	)

)