package ic.swarm.client.guiapp.ui.common.views.buttons


sealed class ButtonState {

	object Active : ButtonState()

	object Inactive : ButtonState()

	object Waiting : ButtonState()

}