package ic.swarm.client.guiapp.ui.common.views.states


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.ByContainer
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.window.Window

import ic.swarm.client.guiapp.ui.common.views.WaitingIndicator


@Suppress("FunctionName")
internal fun Window.WaitingLayout (

	color : Color

) = (

	Stack(
		width = ByContainer, height = ByContainer,
		children = List(
			WaitingIndicator(
				color = color
			)
		)
	)

)