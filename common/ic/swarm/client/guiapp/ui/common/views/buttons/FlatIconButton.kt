package ic.swarm.client.guiapp.ui.common.views.buttons


import ic.graphics.color.Color
import ic.graphics.image.Image

import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.clickable.container.Clickable

import ic.swarm.client.guiapp.ui.common.views.Icon


@Suppress("FunctionName")
fun GuiScope.FlatIconButton (

	icon : Image,

	color : Color,

	onClick : () -> Unit

) = (

	Clickable(
		onClick = onClick,
		child = Icon(
			icon = icon,
			color = color
		)
	)

)