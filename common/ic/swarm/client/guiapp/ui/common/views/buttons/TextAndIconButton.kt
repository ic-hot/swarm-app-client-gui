package ic.swarm.client.guiapp.ui.common.views.buttons


import ic.struct.list.List

import ic.graphics.color.Color
import ic.graphics.color.Color.Companion.Black
import ic.graphics.color.Color.Companion.White
import ic.graphics.image.Image

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.stack.Stack

import ic.swarm.client.guiapp.ui.common.views.buttons.ButtonState.*


@Suppress("FunctionName")
fun GuiScope.Button (

	text : String,

	icon : Image,

	backgroundColor : Color = White,

	contentColor : Color = Black,

	getState : () -> ButtonState = { Active },

	onClick : () -> Unit

) = (

	Material(
		cornersRadius = 8.dp,
		elevation = 4.dp,
		child = Stack(
			width = ByContainer, height = ByContent,
			children = List(
				Solid(color = backgroundColor),
				FlatButton(
					text = text,
					icon = icon,
					contentColor = contentColor,
					getState = getState,
					onClick = onClick
				)
			)
		)
	)

)