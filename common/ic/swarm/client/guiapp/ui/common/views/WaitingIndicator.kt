package ic.swarm.client.guiapp.ui.common.views


import ic.graphics.color.Color

import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.progress.IndeterminateProgressIndicator


@Suppress("FunctionName")
fun GuiScope.WaitingIndicator (

	color : Color

) = (

	IndeterminateProgressIndicator(
		size = 48.dp,
		color = color
	)

)