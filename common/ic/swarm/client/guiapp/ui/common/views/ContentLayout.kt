package ic.swarm.client.guiapp.ui.common.views


import ic.struct.list.List

import ic.gui.control.AbstractDecoratedViewController
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.space.Space
import ic.gui.view.View
import ic.gui.control.ext.BottomDecorSpace
import ic.gui.control.ext.TopDecorSpace


@Suppress("FunctionName")
fun AbstractDecoratedViewController.ContentLayout (

	child : View

) = (

	Column(
		width = ByContainer, height = ByContainer,
		children = List(
			TopDecorSpace(),
			Space(height = 48.dp),
			child,
			BottomDecorSpace()
		)
	)

)