package ic.swarm.client.guiapp.ui.common.views


import ic.base.primitives.float32.Float32
import ic.struct.list.List

import ic.graphics.color.Color
import ic.graphics.image.Image

import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.image.Image
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.image.ScaleMode


@Suppress("FunctionName")
fun GuiScope.Icon (

	icon : Image?,

	color : Color,

	opacity : Float32 = 1F

) = (

	Stack(
		width = 48.dp, height = 48.dp,
		children = List(
			Image(
				width = 24.dp, height = 24.dp,
				scaleMode = ScaleMode.Fit,
				image = icon,
				tint = color,
				opacity = opacity
			)
		)
	)

)