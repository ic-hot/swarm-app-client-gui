package ic.swarm.client.guiapp.ui.common.views.buttons


import ic.struct.list.List
import ic.struct.value.cached.Cached

import ic.graphics.color.Color
import ic.graphics.image.Image

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp

import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.clickable.Clickable
import ic.gui.scope.ext.views.image.Image
import ic.gui.scope.ext.views.progress.IndeterminateProgressIndicator
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.switch.Switch
import ic.gui.scope.ext.views.text.Text


@Suppress("FunctionName")
fun GuiScope.FlatButton (

	text : String,

	icon : Image,

	contentColor : Color = Color.Black,

	getState : () -> ButtonState = { ButtonState.Active },

	onClick : () -> Unit

) = (

	Stack(
		width = ByContainer, height = 48.dp,
		children = List(
			Switch(
				width = ByContainer, height = ByContainer,
				child = Cached(getState) { state ->
					when (state) {
						ButtonState.Waiting -> IndeterminateProgressIndicator(
							size = 24.dp,
							color = contentColor
						)
						else -> Row(
							width = ByContainer, height = ByContainer,
							getOpacity = { if (getState() == ButtonState.Active) 1F else .5F },
							children = List(
								Space(12.dp),
								Image(
									width = ByContent, height = ByContent,
									image = icon,
									tint = contentColor
								),
								Space(12.dp),
								Text(
									width = ByContainer,
									size = 14.dp,
									value = text,
									color = contentColor
								),
								Space(12.dp)
							)
						)
					}
				}
			),
			Clickable(
				isClickEnabled = { getState() == ButtonState.Active },
				onClick = onClick
			)
		)
	)

)