package ic.swarm.client.guiapp.ui.common.views.decor


import ic.graphics.color.Color

import ic.gui.align.Bottom
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.window.Window


@Suppress("FunctionName")
fun Window.BottomBar (

	color : Color

) = (

	Material(
		verticalAlign = Bottom,
		elevation = 8.dp,
		child = Solid(
			getHeight = { bottomDecorHeight },
			color = color
		)
	)

)