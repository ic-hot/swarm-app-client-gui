package ic.swarm.client.guiapp.ui.common.views.buttons


import ic.struct.list.List
import ic.struct.value.cached.Cached

import ic.graphics.color.Color
import ic.graphics.color.Color.Companion.Black
import ic.graphics.color.Color.Companion.White

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.clickable.container.Clickable
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.progress.IndeterminateProgressIndicator
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.switch.Switch
import ic.gui.scope.ext.views.text.Text

import ic.swarm.client.guiapp.ui.common.views.buttons.ButtonState.*


@Suppress("FunctionName")
fun GuiScope.Button (

	text : String,

	color : Color = White,

	getState : () -> ButtonState = { Active },

	onClick : () -> Unit

) = (

	Clickable(
		child = Material(
			cornersRadius = 8.dp,
			elevation = 4.dp,
			child = Stack(
				width = ByContainer, height = 48.dp,
				children = List(
					Solid(color = color),
					Switch(
						width = ByContainer, height = ByContainer,
						child = Cached(getState) { state ->
							when (state) {
								Waiting -> IndeterminateProgressIndicator(
									size = 24.dp,
									color = Black
								)
								else -> Text(
									width = ByContent,
									size = 14.dp,
									value = text,
									color = Black,
									getOpacity = {
										if (getState() == Active) 1F else .5F
									}
								)
							}
						}
					)

				)
			)
		),
		toEnableClick = {
			getState() == Active
		},
		onClick = onClick
	)

)