package ic.swarm.client.guiapp.ui.common


import ic.base.primitives.float32.ext.toString

import ic.swarm.client.guiapp.global.swarmStatus
import ic.swarm.status.SwarmStatus


val networkCoverageValueText : String get() {

	val status = swarmStatus

	if (status is SwarmStatus.Running) {

		return status.networkCoverage.toString(pattern = "0.##")

	} else {

		return ""

	}

}