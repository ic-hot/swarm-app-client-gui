package ic.swarm.client.guiapp.ui.windows


object WindowType {

	const val Main : Byte = 1

	const val Service : Byte = 2

	const val InitHomeSwarmDirectory : Byte = 3

	const val Folder    : Byte = 4
	const val PlainText : Byte = 5
	const val PngImage  : Byte = 6

	const val Settings : Byte = 7

}