package ic.swarm.client.guiapp.ui.service


import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.input.ext.getNextByte
import ic.stream.output.ByteOutput

import ic.gui.window.notification.NotificationWindow

import ic.swarm.client.guiapp.global.isServiceShouldBeRunning
import ic.swarm.client.guiapp.ui.broadcast.MessageType.StopService
import ic.swarm.client.guiapp.ui.broadcast.sendServiceStateChangedBroadcast
import ic.swarm.client.guiapp.ui.service.impl.startService
import ic.swarm.client.guiapp.ui.service.impl.stopService
import ic.swarm.client.guiapp.ui.windows.WindowType


class ServiceWindow : NotificationWindow() {


	override val isCloseable get() = false


	private val thisServiceWindow get() = this


	override val title get() = "Swarm node service"

	override val message get() = "Node is active"


	override fun ByteOutput.writeTargetWindowTypeAndState() {
		putByte(WindowType.Main)
	}


	override fun onOpen() {
		isServiceShouldBeRunning = true
		sendServiceStateChangedBroadcast()
		startService()
	}


	@Throws(IoException::class)
	override fun ByteInput.readBroadcast() {
		val messageType = getNextByte()
		when (messageType) {
			StopService -> {
				thisServiceWindow.close()
			}
		}
	}


	@Throws(IoException::class)
	override fun onClose() {
		stopService()
		isServiceShouldBeRunning = false
		sendServiceStateChangedBroadcast()
	}


}