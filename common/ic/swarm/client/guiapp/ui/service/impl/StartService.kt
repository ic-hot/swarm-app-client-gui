package ic.swarm.client.guiapp.ui.service.impl


import ic.design.task.scope.ext.listenNetworkAvailability
import ic.ifaces.stoppable.ext.stopNonBlockingIfNeeded
import ic.network.avail.isNetworkAvailable
import ic.service.ext.startNonBlockingIfNeeded

import ic.swarm.client.guiapp.global.swarm
import ic.swarm.client.guiapp.global.swarmStatus
import ic.swarm.client.guiapp.ui.service.ServiceWindow
import ic.swarm.status.SwarmStatus


internal fun ServiceWindow.startService() {

	if (!isNetworkAvailable) {
		swarmStatus = SwarmStatus.Idle
	}

	listenNetworkAvailability(
		toCallAtOnce = true,
		onConnectionEstablished = {
			swarm.startNonBlockingIfNeeded()
		},
		onConnectionLost = {
			swarm.stopNonBlockingIfNeeded()
		}
	)

}