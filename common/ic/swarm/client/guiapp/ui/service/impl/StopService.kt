package ic.swarm.client.guiapp.ui.service.impl


import ic.ifaces.stoppable.ext.stopNonBlocking

import ic.swarm.client.guiapp.global.swarm
import ic.swarm.client.guiapp.ui.service.ServiceWindow


internal fun ServiceWindow.stopService() {

	swarm.stopNonBlocking()

}