package ic.swarm.client.guiapp.ui.service


import ic.gui.window.scope.abstr.AbstractWindowScope
import ic.gui.window.scope.ext.openNotificationWindow

import ic.swarm.client.guiapp.ui.windows.WindowType


fun AbstractWindowScope.openServiceWindow() {

	openNotificationWindow {
		putByte(WindowType.Service)
	}

}