package ic.swarm.client.guiapp.ui.broadcast


import ic.gui.broadcast.broadcast


fun sendServiceStateChangedBroadcast() {

	broadcast {

		putByte(MessageType.ServiceStateChanged)

	}

}