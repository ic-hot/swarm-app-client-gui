package ic.swarm.client.guiapp.ui.broadcast


import ic.struct.list.List
import ic.stream.output.ext.writeListWithInt32Length
import ic.stream.output.ext.writeString

import ic.gui.broadcast.broadcast


fun broadcastUpdateFolder (path: List<String>) {

	broadcast {
		putByte(MessageType.UpdateFolder)
		writeListWithInt32Length(path) {
			writeString(it)
		}
	}

}