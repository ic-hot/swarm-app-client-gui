package ic.swarm.client.guiapp.ui.broadcast


object MessageType {

	const val ServiceStateChanged : Byte = 1

	const val StopService : Byte = 3

	const val UpdateFolder : Byte = 4

}