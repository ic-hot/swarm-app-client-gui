package ic.swarm.client.guiapp.ui.settings


import ic.gui.window.Window

import ic.swarm.client.guiapp.ui.settings.view.SettingsView


class SettingsWindow : Window() {

	override fun initView() = SettingsView()

}