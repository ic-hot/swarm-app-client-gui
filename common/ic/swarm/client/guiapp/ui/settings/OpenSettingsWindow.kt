package ic.swarm.client.guiapp.ui.settings


import ic.gui.window.Window
import ic.gui.window.scope.ext.openWindow

import ic.swarm.client.guiapp.ui.windows.WindowType


fun Window.openSettingsWindow() {

	openWindow {
		putByte(WindowType.Settings)
	}

}