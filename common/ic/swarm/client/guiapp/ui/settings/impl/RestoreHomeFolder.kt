package ic.swarm.client.guiapp.ui.settings.impl


import ic.base.throwables.UnableToParseException

import ic.swarm.access.AccessKey
import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.link.ext.fromUrlOrThrowUnableToParse
import ic.swarm.fs.type.ContentType

import ic.swarm.client.guiapp.io.fs.setHomeFolder
import ic.swarm.client.guiapp.ui.common.popups.enterpassword.showEnterPasswordPopup
import ic.swarm.client.guiapp.ui.settings.SettingsWindow


fun SettingsWindow.restoreHomeFolder() {

	showAlert(
		message = (
			"Are you sure you want to REPLACE current home folder with folder from link?"
		),
		yesButtonText = "Yes",
		onClickYes = {
			val linkAsText = getStringFromClipboard()
			if (linkAsText == null) {
				showToast("Clipboard is empty")
			} else {
				val link = (
					try {
						SwarmLink.fromUrlOrThrowUnableToParse(linkAsText)
					} catch (_: UnableToParseException) {
						showToast("String in clipboard is not a Swarm link")
						return@showAlert
					}
				)
				if (link.contentType != ContentType.Folder) {
					showToast("Link is not referring to a folder")
					return@showAlert
				}
				when (link.access) {
					ic.swarm.access.Access.Everyone -> {
						setHomeFolder(
							streamAddress = link.streamAddress,
							accessKey = AccessKey.None
						)
						showToast("Restored from clipboard")
					}
					ic.swarm.access.Access.Restricted.WhoeverHasPassword -> {
						showEnterPasswordPopup(
							onCloseWithResult = { password ->
								setHomeFolder(
									streamAddress = link.streamAddress,
									accessKey = AccessKey.Private.Password(password = password)
								)
								showToast("Restored from clipboard")
							},
							onCloseWithoutResult = {}
						)
					}
				}
			}
		},
		noButtonText = "No",
		onClickNo = {}
	)

}