package ic.swarm.client.guiapp.ui.settings.impl


import ic.swarm.client.guiapp.io.fs.getHomeFolderLink
import ic.swarm.client.guiapp.ui.settings.SettingsWindow


fun SettingsWindow.backupHomeFolder() {

	val link = getHomeFolderLink()

	if (link == null) {

		showToast("No home folder set")

	} else {

		copyToClipboard(
			link.toString()
		)
		showToast("Copied to clipboard")

	}

}