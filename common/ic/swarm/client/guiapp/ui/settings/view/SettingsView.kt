package ic.swarm.client.guiapp.ui.settings.view


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.control.ext.BottomDecorSpace
import ic.gui.control.ext.TopDecorSpace
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.res.icons.material.MaterialIcon

import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.common.views.buttons.ButtonGroup
import ic.swarm.client.guiapp.ui.common.views.buttons.FlatButton
import ic.swarm.client.guiapp.ui.settings.SettingsWindow
import ic.swarm.client.guiapp.ui.settings.impl.backupHomeFolder
import ic.swarm.client.guiapp.ui.settings.impl.restoreHomeFolder


@Suppress("FunctionName")
internal fun SettingsWindow.SettingsView() = (

	Stack(

		Solid(theme.backgroundColor),

		Column(

			TopDecorSpace(),

			Padding(
				padding = 12.dp,
				child = Column(
					height = ByContent,
					children = List(
						ButtonGroup(
							backgroundColor = Color.White,
							children = List(
								FlatButton(
									icon = MaterialIcon.Link,
									text = "Backup home folder",
									contentColor = Color.Black,
									onClick = {
										backupHomeFolder()
									}
								),
								FlatButton(
									icon = MaterialIcon.ContentPaste,
									text = "Restore home folder",
									contentColor = Color.Black,
									onClick = {
										restoreHomeFolder()
									}
								)
							)
						)
					)
				)
			),

			Space(),

			BottomDecorSpace()

		)

	)

)