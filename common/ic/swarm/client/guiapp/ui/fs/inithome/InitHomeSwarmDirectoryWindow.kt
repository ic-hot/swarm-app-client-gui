package ic.swarm.client.guiapp.ui.fs.inithome


import ic.gui.window.Window

import ic.swarm.client.guiapp.ui.fs.inithome.view.InitHomeSwarmDirectoryScreen


internal class InitHomeSwarmDirectoryWindow : Window() {

	override val isTopDecorLight    get() = true
	override val isBottomDecorLight get() = true

	override fun initView() = InitHomeSwarmDirectoryScreen()

	var isCreatingEmpty    : Boolean = false
	var isCreatingFromLink : Boolean = false

	val isWaiting : Boolean get() = isCreatingEmpty || isCreatingFromLink

	override fun onClose() {

	}

}