package ic.swarm.client.guiapp.ui.fs.inithome.impl


import ic.base.throwables.IoException
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread
import ic.struct.list.List
import ic.swarm.access.AccessKey
import ic.swarm.client.guiapp.io.fs.setHomeFolderToEmpty
import ic.swarm.client.guiapp.model.addr.FsAddress

import ic.swarm.client.guiapp.ui.common.popups.createpassword.showCreatePasswordPopup
import ic.swarm.client.guiapp.ui.fs.folder.openFolderWindow
import ic.swarm.client.guiapp.ui.fs.inithome.InitHomeSwarmDirectoryWindow


internal fun InitHomeSwarmDirectoryWindow.onClickCreateEmpty() {

	if (isWaiting) return

	showCreatePasswordPopup(
		onCloseWithoutResult = {},
		onCloseWithResult = { password ->

			isCreatingEmpty = true
			updateView()

			doInBackground {

				try {

					val accessKey = AccessKey.Private.Password(password)

					setHomeFolderToEmpty(
						accessKey = accessKey
					)

					openFolderWindow(
						title = "Home folder",
						address = FsAddress.InHomeFolder(
							path = List()
						),
						ownAccessKey = accessKey
					)

					close()

				} catch (_: IoException) {

					doInUiThread {
						showToast("Error creating empty Swarm folder")
					}

				} finally {

					doInUiThread {
						isCreatingEmpty = false
						updateView()
					}

				}

			}

		}
	)

}