package ic.swarm.client.guiapp.ui.fs.inithome.view


import ic.struct.list.List

import ic.graphics.color.Color.Companion.Black
import ic.graphics.color.Color.Companion.White

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.text.Text
import ic.gui.control.ext.BottomDecorSpace
import ic.gui.control.ext.TopDecorSpace
import ic.swarm.client.guiapp.ui.common.theme.theme

import ic.swarm.client.guiapp.ui.fs.inithome.InitHomeSwarmDirectoryWindow
import ic.swarm.client.guiapp.ui.common.views.buttons.Button
import ic.swarm.client.guiapp.ui.common.views.buttons.ButtonState.*
import ic.swarm.client.guiapp.ui.fs.inithome.impl.onClickCreateEmpty


@Suppress("FunctionName")
internal fun InitHomeSwarmDirectoryWindow.InitHomeSwarmDirectoryScreen() = (

	Stack(
		width = ByContainer, height = ByContainer,
		children = List(
			Solid(color = White),
			Column(
				width = ByContainer, height = ByContainer,
				children = List(
					TopDecorSpace(),
					Padding(
						padding = 16.dp,
						child = Column(
							width = ByContainer, height = ByContainer,
							children = List(
								Text(
									width = ByContainer,
									size = 24.dp,
									value = "You don't have a home folder",
									color = Black
								),
								Space(),
								Button(
									text = "Create empty Swarm folder",
									color = theme.folderColor,
									getState = { if (isCreatingEmpty) Waiting else Active },
									onClick = ::onClickCreateEmpty
								),
								Space(height = 16.dp),
								Button(
									text = "Use existing Swarm folder",
									color = theme.folderColor,
									getState = { if (isCreatingFromLink) Waiting else Active },
									onClick = {
										// TODO
									}
								)
							)
						)
					),
					BottomDecorSpace()
				)
			)
		)
	)

)