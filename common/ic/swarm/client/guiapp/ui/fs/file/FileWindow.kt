package ic.swarm.client.guiapp.ui.fs.file


import ic.base.annotations.Blocking
import ic.base.throwables.AccessException
import ic.base.throwables.IoException
import ic.base.throwables.NotSupportedException
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.stream.input.ByteInput
import ic.stream.output.ByteOutput
import ic.struct.list.List
import ic.struct.list.ext.copy.copyRange
import ic.struct.list.ext.length.last

import ic.graphics.color.Color

import ic.res.icons.material.MaterialIcon

import ic.swarm.access.AccessKey
import ic.swarm.fs.entry.SwarmFile
import ic.swarm.fs.entry.SwarmFsEntry
import ic.swarm.fs.entry.ext.createFileIfNotExists
import ic.swarm.fs.entry.ext.recursiveGetFolder
import ic.swarm.fs.type.ContentType
import ic.swarm.ext.fs.getFile

import ic.swarm.client.guiapp.global.swarm
import ic.swarm.client.guiapp.io.fs.getHomeFolder
import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.client.guiapp.ui.common.views.buttons.FlatIconButton
import ic.swarm.client.guiapp.ui.fs.FsWindow
import ic.swarm.client.guiapp.ui.fs.file.impl.save
import ic.swarm.client.guiapp.ui.fs.impl.isInHomeFolder


internal abstract class FileWindow<Content: Any> : FsWindow<Content>() {


	abstract val contentType : ContentType.File


	@Throws(IoException::class)
	abstract fun ByteInput.readContent() : Content


	override fun initEditActionButtons() = List(
		FlatIconButton(
			icon = MaterialIcon.Check,
			color = Color.Black,
			onClick = {
				save()
			}
		)
	)


	@Throws(IoException::class)
	abstract fun ByteOutput.writeContent (content: Content)


	internal val file get() = entryOrNull as SwarmFile


	@Blocking
	@Throws(IoException::class, AccessException::class, NotSupportedException::class)
	override fun initEntry() : SwarmFsEntry {
		val address = this.address
		when (address) {
			is FsAddress.InHomeFolder -> {
				return getHomeFolder()!!.recursiveGetFolder(
					address.path.copyRange(0, address.path.length - 1)
				).createFileIfNotExists(
					ownAccessKey = address.path.last.accessKey,
					name = address.path.last.name,
					contentType = contentType,
					childOwnAccessKey = ownAccessKey
				)
			}
			is FsAddress.NotInHomeFolder -> {
				return swarm.getFile(
					streamAddress = address.streamAddress,
					access = address.access,
					contentType = contentType
				)
			}
		}
	}


	@Blocking
	@Throws(IoException::class, AccessException::class)
	override fun initContent (entry: SwarmFsEntry, ownAccessKey: AccessKey?) : Content {
		return file.openInput(
			ownAccessKey = ownAccessKey
		).runAndCloseOrCancel {
			readContent()
		}
	}


}