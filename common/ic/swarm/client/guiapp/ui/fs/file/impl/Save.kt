package ic.swarm.client.guiapp.ui.fs.file.impl


import ic.base.throwables.IoException
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.ext.copy.copyRange

import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.client.guiapp.ui.broadcast.broadcastUpdateFolder
import ic.swarm.client.guiapp.ui.fs.State
import ic.swarm.client.guiapp.ui.fs.file.FileWindow


internal fun <Content: Any> FileWindow<Content>.save() {

	val thisWindow = this

	val content = this.content!!

	state = State.Loading
	updateView()

	doInBackground {

		try {

			file.openOutput(
				newThumbnail = null
			).runAndCloseOrCancel {
				writeContent(content)
			}

			doInUiThread {

				state = State.Loaded
				updateView()

				showToast("Saved")

				val address = thisWindow.address as FsAddress.InHomeFolder
				val parentPath = (
					address.path
					.copyRange(0, address.path.length - 1)
					.copyConvert { it.name }
				)

				broadcastUpdateFolder(
					path = parentPath
				)

			}

		} catch (_: IoException) {

			doInUiThread {
				state = State.Loaded
				updateView()
				showToast("Can't save file")
			}

		}

	}

}