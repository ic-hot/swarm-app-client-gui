package ic.swarm.client.guiapp.ui.fs.png


import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.input.ext.read
import ic.stream.output.ByteOutput

import ic.graphics.image.Image
import ic.graphics.image.ext.fromPng

import ic.gui.dim.dp.ByContainer
import ic.gui.scope.ext.views.clickable.container.Clickable
import ic.gui.scope.ext.views.image.Image

import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.fs.file.FileWindow
import ic.swarm.fs.type.ContentType


internal class PngImageWindow : FileWindow<Image>() {


	override val contentType get() = ContentType.File.PngImage

	override val isContentEditable get() = false


	override val accentColor get() = theme.imageColor


	override fun initLoadedContentLayout() = (
		Clickable(
			child = Image(
				width = ByContainer, height = ByContainer,
				getImage = { content }
			),
			onClick = {
				goToImageViewer(content!!)
				close()
			}
		)
	)


	@Throws(IoException::class)
	override fun ByteInput.readContent() : Image {
		return Image.fromPng(
			read()
		)
	}


	@Throws(IoException::class)
	override fun ByteOutput.writeContent (content: Image) {
		throw RuntimeException()
	}


}