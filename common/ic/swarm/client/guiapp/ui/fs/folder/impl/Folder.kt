package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.swarm.fs.entry.SwarmFolder

import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow


internal val FolderWindow.folder get() = entryOrNull as SwarmFolder