package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.base.throwables.IoException
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread

import ic.swarm.fs.entry.SwarmFolder
import ic.swarm.fs.entry.SwarmFsEntry
import ic.swarm.fs.entry.ext.removeIfExists

import ic.swarm.client.guiapp.ui.fs.State.*
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow
import ic.swarm.client.guiapp.ui.fs.impl.loadContent


internal fun FolderWindow.removeItem (item: SwarmFsEntry) {

	val folder = entryOrNull as SwarmFolder

	state = Loading
	updateView()

	val ownAccessKey = ownAccessKey

	doInBackground {

		try {

			folder.removeIfExists(
				ownAccessKey = ownAccessKey,
				name = item.location!!.name
			)

			doInUiThread {
				loadContent()
			}

		} catch (_: IoException) {

			state = ConnectionFailure

		}

	}

}