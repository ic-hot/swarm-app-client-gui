package ic.swarm.client.guiapp.ui.fs.folder.view


import ic.base.throwables.NotSupportedException
import ic.struct.list.List

import ic.graphics.color.Color.Companion.Black
import ic.graphics.color.Color.Companion.White

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.clickable.container.Clickable
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.text.Text

import ic.res.icons.material.MaterialIcon

import ic.swarm.access.Access.Everyone
import ic.swarm.access.Access.Restricted.WhoeverHasPassword
import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.common.views.Icon
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow
import ic.swarm.client.guiapp.ui.fs.folder.impl.onClickItem
import ic.swarm.client.guiapp.ui.fs.folder.impl.onSecondaryClickItem
import ic.swarm.fs.entry.SwarmFsEntry
import ic.swarm.fs.type.ContentType


@Suppress("FunctionName")
internal fun FolderWindow.ItemView (item: SwarmFsEntry) = (

	Padding(
		top = 4.dp,
		child = Clickable(
			onClick = { onClickItem(item) },
			onSecondaryClick = { onSecondaryClickItem(item) },
			child = Material(
				elevation = 4.dp,
				child = Stack(
					width = ByContainer, height = 48.dp,
					children = List(
						Solid(color = White),
						Row(
							width = ByContainer, height = ByContainer,
							children = List(
								Icon(
									icon = when (item.contentType) {
										ContentType.Folder -> MaterialIcon.Folder
										ContentType.File.PlainText -> MaterialIcon.FileDocumentOutline
										ContentType.File.PngImage -> MaterialIcon.FileImageOutline
										else -> throw NotSupportedException.Runtime(
											"entry.type: ${ item.contentType }"
										)
									},
									color = when (item.contentType) {
										ContentType.Folder -> theme.folderColor
										ContentType.File.PlainText -> theme.plainTextColor
										ContentType.File.PngImage -> theme.imageColor
										else -> throw NotSupportedException.Runtime(
											"entry.type: ${ item.contentType }"
										)
									}
								),
								Text(
									width = ByContainer,
									size = 14.dp,
									getValue = { item.location!!.name },
									color = Black
								),
								Icon(
									icon = when (item.ownAccess) {
										null -> null
										Everyone -> MaterialIcon.LockOpenOutline
										WhoeverHasPassword -> MaterialIcon.LockOutline
									},
									color = Black,
									opacity = .25F
								)
							)
						)
					)
				)
			)
		)
	)

)