package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.swarm.access.AccessKey


data class FileToCreate (

	val name : String,

	val ownAccessKey : AccessKey?

)