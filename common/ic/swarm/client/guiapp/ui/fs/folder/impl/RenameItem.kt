package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.base.throwables.IoException
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread

import ic.swarm.client.guiapp.ui.fs.State.*
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow
import ic.swarm.client.guiapp.ui.fs.impl.loadContent
import ic.swarm.fs.entry.SwarmFsEntry
import ic.swarm.fs.entry.ext.renameOrThrowAlreadyExists


internal fun FolderWindow.renameItem (item: SwarmFsEntry, newName: String) {

	state = Loading
	updateView()

	doInBackground {

		try {

			item.renameOrThrowAlreadyExists(newName = newName)

			doInUiThread {
				loadContent()
			}

		} catch (_: IoException) {

			doInUiThread {
				state = ConnectionFailure
				updateView()
			}

		}

	}

}