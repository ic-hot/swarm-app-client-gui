package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.struct.list.ext.reduce.find.atLeastOne

import ic.swarm.access.AccessKey

import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow


internal fun FolderWindow.createImage (name: String, childOwnAccessKey: AccessKey?) {

	val items = content

	val alreadyExists = items!!.atLeastOne { it.location!!.name == name }

	if (alreadyExists) {
		showToast("Item $name already exists")
		return
	}

	fileToCreate = FileToCreate(
		name = name,
		ownAccessKey = childOwnAccessKey
	)

	openImageSelector("Select image")

}