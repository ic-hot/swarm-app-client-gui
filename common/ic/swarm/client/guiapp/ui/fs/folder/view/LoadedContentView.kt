package ic.swarm.client.guiapp.ui.fs.folder.view


import ic.struct.list.cache.CacheConvertList
import ic.struct.list.ext.length.isEmpty
import ic.struct.value.cached.Cached

import ic.gui.align.Top
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.switch.Switch
import ic.gui.scope.ext.views.vscroll.VerticalScroll

import ic.swarm.client.guiapp.ui.common.views.states.ErrorLayout
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow


@Suppress("FunctionName")
internal fun FolderWindow.LoadedContentView() = (

	Switch(
		child = Cached({ content!!.isEmpty }) {

			if (content!!.isEmpty) {

				ErrorLayout(
					text = "Folder is empty"
				)

			} else {

				VerticalScroll(
					child = Column(
						width = ByContainer, height = ByContent,
						verticalAlign = Top,
						children = CacheConvertList({ content!! }) { entry ->

							ItemView(entry)

						}
					)
				)

			}

		}
	)

)