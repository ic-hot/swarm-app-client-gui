package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.struct.list.ext.plus
import ic.struct.list.ext.reduce.find.atLeastOne

import ic.swarm.access.AccessKey
import ic.swarm.fs.path.AccessKeyAndName

import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.client.guiapp.ui.fs.txt.openPlainTextWindow
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow


internal fun FolderWindow.createPlainText (name: String, childOwnAccessKey: AccessKey?) {

	val address = this.address as FsAddress.InHomeFolder

	val path = address.path

	val items = content

	val alreadyExists = items!!.atLeastOne { it.location!!.name == name }

	if (alreadyExists) {

		showToast("Item $name already exists")

	} else {

		openPlainTextWindow(
			title = name,
			address = FsAddress.InHomeFolder(
				path = path + AccessKeyAndName(ownAccessKey, name)
			),
			accessKey = childOwnAccessKey
		)

	}

}