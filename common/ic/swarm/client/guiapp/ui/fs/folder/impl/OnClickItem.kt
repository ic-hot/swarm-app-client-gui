package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.base.throwables.NotSupportedException
import ic.struct.list.ext.plus

import ic.swarm.fs.entry.SwarmFsEntry
import ic.swarm.fs.path.AccessKeyAndName
import ic.swarm.fs.type.ContentType

import ic.swarm.client.guiapp.model.addr.FsAddress.*
import ic.swarm.client.guiapp.ui.fs.folder.openFolderWindow
import ic.swarm.client.guiapp.ui.fs.txt.openPlainTextWindow
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow
import ic.swarm.client.guiapp.ui.fs.png.openPngImageWindow


internal fun FolderWindow.onClickItem (item: SwarmFsEntry) {

	val title = item.location!!.name

	val address = this.address

	val childAddress = (
		when (address) {
			is NotInHomeFolder -> {
				NotInHomeFolder(
					streamAddress = item.streamAddress,
					access = item.access
				)
			}
			is InHomeFolder -> {
				InHomeFolder(
					path = address.path + AccessKeyAndName(ownAccessKey, item.location!!.name)
				)
			}
		}
	)

	when (item.contentType) {

		ContentType.Folder -> {
			openFolderWindow(
				title = title,
				address = childAddress
			)
		}

		ContentType.File.PlainText -> {
			openPlainTextWindow(
				title = title,
				address = childAddress
			)
		}

		ContentType.File.PngImage -> {
			openPngImageWindow(
				title = title,
				address = childAddress
			)
		}

		else -> throw NotSupportedException.Runtime("entry.type: ${ item.contentType }")

	}

}