package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.swarm.client.guiapp.ui.common.popups.itemmenu.showItemMenuPopup
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow
import ic.swarm.client.guiapp.ui.fs.impl.isEditable
import ic.swarm.fs.entry.SwarmFsEntry


internal fun FolderWindow.onSecondaryClickItem (item: SwarmFsEntry) {

	showItemMenuPopup(
		item = item,
		isEditable = isEditable,
		removeItem = { removeItem(item) },
		renameItem = { newName -> renameItem(item, newName) }
	)

}