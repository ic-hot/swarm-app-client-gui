package ic.swarm.client.guiapp.ui.fs.folder


import ic.stream.output.ext.writeString

import ic.gui.window.Window
import ic.gui.window.scope.ext.openWindow
import ic.swarm.access.AccessKey

import ic.swarm.access.funs.writeNullableAccessKey

import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.client.guiapp.model.addr.funs.writeFsAddress
import ic.swarm.client.guiapp.ui.windows.WindowType


fun Window.openFolderWindow (

	title : String,

	address : FsAddress,

	ownAccessKey : ic.swarm.access.AccessKey? = null

) {

	openWindow {
		putByte(WindowType.Folder)
		writeString(title)
		writeFsAddress(address)
		writeNullableAccessKey(ownAccessKey)
	}

}