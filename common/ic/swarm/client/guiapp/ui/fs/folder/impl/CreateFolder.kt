package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.IoException
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread

import ic.swarm.access.AccessKey
import ic.swarm.client.guiapp.ui.fs.State
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow
import ic.swarm.client.guiapp.ui.fs.impl.load
import ic.swarm.fs.entry.SwarmFolder
import ic.swarm.fs.entry.ext.createFolderOrThrowAlreadyExists


internal fun FolderWindow.createFolder (name: String, childOwnAccessKey: AccessKey?) {

	val folder = entryOrNull as SwarmFolder

	state = State.Loading
	updateView()

	doInBackground {

		try {
			folder.createFolderOrThrowAlreadyExists(
				ownAccessKey = ownAccessKey,
				name = name,
				childOwnAccessKey = childOwnAccessKey
			)
			doInUiThread {
				load()
			}
		} catch (_: AlreadyExistsException) {
			doInUiThread {
				showToast("Item $name already exists")
				state = State.Loaded
				updateView()
			}
		} catch (_: IoException) {
			doInUiThread {
				state = State.ConnectionFailure
				updateView()
			}
		}

	}

}