package ic.swarm.client.guiapp.ui.fs.folder


import ic.base.annotations.Blocking
import ic.base.throwables.AccessException
import ic.base.throwables.IoException
import ic.base.throwables.NotSupportedException
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.stream.input.ByteInput
import ic.stream.input.ext.getNextByte
import ic.stream.input.ext.readListWithInt32Length
import ic.stream.input.ext.readString
import ic.struct.list.ext.copy.convert.copyConvert
import ic.struct.list.List
import ic.struct.collection.ext.copy.copySortToList
import ic.util.log.logW

import ic.graphics.color.Color
import ic.graphics.image.Image

import ic.gui.view.View

import ic.res.icons.material.MaterialIcon

import ic.swarm.access.AccessKey
import ic.swarm.ext.fs.getFolder
import ic.swarm.fs.entry.SwarmFolder
import ic.swarm.fs.entry.SwarmFsEntry
import ic.swarm.fs.entry.ext.createFileIfNotExists
import ic.swarm.fs.entry.ext.getChildren
import ic.swarm.fs.entry.ext.recursiveGetEntry
import ic.swarm.fs.type.ContentType

import ic.swarm.client.guiapp.global.swarm
import ic.swarm.client.guiapp.io.fs.getHomeFolder
import ic.swarm.client.guiapp.model.addr.FsAddress.*
import ic.swarm.client.guiapp.ui.broadcast.MessageType.UpdateFolder
import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.common.views.buttons.FlatIconButton
import ic.swarm.client.guiapp.ui.fs.FsWindow
import ic.swarm.client.guiapp.ui.fs.State.*
import ic.swarm.client.guiapp.ui.fs.folder.impl.FileToCreate
import ic.swarm.client.guiapp.ui.fs.folder.impl.folder
import ic.swarm.client.guiapp.ui.fs.folder.impl.onClickAddFile
import ic.swarm.client.guiapp.ui.fs.folder.view.LoadedContentView
import ic.swarm.client.guiapp.ui.fs.impl.isInHomeFolder
import ic.swarm.client.guiapp.ui.fs.impl.load
import ic.swarm.client.guiapp.ui.fs.impl.loadContent
import ic.util.log.logD


internal class FolderWindow : FsWindow<List<SwarmFsEntry>>() {


	override val thisWindow get() = this


	override val accentColor get() = theme.folderColor


	override val isContentEditable get() = true

	override fun initEditActionButtons() : List<View> = List(
		FlatIconButton(
			icon = MaterialIcon.Plus,
			color = Color.Black,
			onClick = ::onClickAddFile
		)
	)


	override fun initLoadedContentLayout() = LoadedContentView()


	@Throws(IoException::class)
	override fun ByteInput.readBroadcast() {
		val messageType = getNextByte()
		when (messageType) {
			UpdateFolder -> {
				val thisPath = (address as InHomeFolder).path.copyConvert { it.name }
				val targetPath = readListWithInt32Length { readString() }
				if (thisPath == targetPath) {
					state = Loading
					updateView()
					loadContent()
				}
			}
		}
	}


	@Blocking
	@Throws(IoException::class, AccessException::class, NotSupportedException::class)
	override fun initEntry() : SwarmFsEntry {
		val address = this.address
		when (address) {
			is InHomeFolder -> {
				return getHomeFolder()!!.recursiveGetEntry(address.path)
			}
			is NotInHomeFolder -> {
				return swarm.getFolder(
					streamAddress = address.streamAddress,
					access = address.access
				)
			}
		}
	}


	@Blocking
	@Throws(IoException::class, AccessException::class, NotSupportedException::class)
	override fun initContent (
		entry: SwarmFsEntry, ownAccessKey: AccessKey?
	) : List<SwarmFsEntry> {
		val folder = entry as SwarmFolder
		val children = folder.getChildren(
			ownAccessKey = ownAccessKey
		)
		return children.keys.copySortToList().copyConvert { children[it]!! }
	}


	internal var fileToCreate : FileToCreate? = null

	override fun onImageSelected (image: Image) {
		val fileToCreate = this.fileToCreate!!.also { this.fileToCreate = null }
		state = Loading
		updateView()
		val folder = this.folder
		doInBackground {
			folder.createFileIfNotExists(
				ownAccessKey = ownAccessKey,
				name = fileToCreate.name,
				contentType = ContentType.File.PngImage,
				childOwnAccessKey = fileToCreate.ownAccessKey
			).openOutput(
				newThumbnail = null
			).runAndCloseOrCancel {
				write(
					image.toPng()
				)
			}
			doInUiThread {
				load()
			}
		}
	}


}