package ic.swarm.client.guiapp.ui.fs.folder.impl


import ic.swarm.client.guiapp.ui.common.popups.additem.showAddItemPopup
import ic.swarm.client.guiapp.ui.fs.folder.FolderWindow


internal fun FolderWindow.onClickAddFile() {

	showAddItemPopup(
		createFolder = ::createFolder,
		createImage = ::createImage,
		createPlainText = ::createPlainText,
		pasteLink = ::pasteLink
	)

}