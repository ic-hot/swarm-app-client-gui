package ic.swarm.client.guiapp.ui.fs


sealed class State {

	object Loading : State()

	object ConnectionFailure : State()

	object NotSupportedError : State()

	object Loaded : State()

}