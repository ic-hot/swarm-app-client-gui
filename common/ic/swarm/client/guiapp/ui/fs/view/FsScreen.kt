package ic.swarm.client.guiapp.ui.fs.view


import ic.struct.list.List

import ic.graphics.color.Color

import ic.gui.dim.dp.ByContainer
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.stack.Stack

import ic.swarm.client.guiapp.ui.common.views.ContentLayout
import ic.swarm.client.guiapp.ui.common.views.decor.BottomBar
import ic.swarm.client.guiapp.ui.common.views.decor.TopBar
import ic.swarm.client.guiapp.ui.fs.FsWindow


@Suppress("FunctionName")
internal fun <Content: Any> FsWindow<Content>.FsScreen() = (

	Stack(
		width = ByContainer, height = ByContainer,
		children = List(
			Solid(color = Color.White),
			ContentLayout(
				child = ContentLayout()
			),
			TopBar(
				titleText = titleText,
				color = accentColor,
				rightViews = initEditActionButtonsOrEmptyList()
			),
			BottomBar(
				color = accentColor
			)
		)
	)

)