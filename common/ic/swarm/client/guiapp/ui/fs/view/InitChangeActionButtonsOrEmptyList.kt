package ic.swarm.client.guiapp.ui.fs.view


import ic.struct.list.List

import ic.gui.view.View

import ic.swarm.client.guiapp.ui.fs.FsWindow
import ic.swarm.client.guiapp.ui.fs.impl.isEditable


internal fun FsWindow<*>.initEditActionButtonsOrEmptyList() : List<out View> {

	if (isEditable) {

		return initEditActionButtons()

	} else {

		return List()

	}

}