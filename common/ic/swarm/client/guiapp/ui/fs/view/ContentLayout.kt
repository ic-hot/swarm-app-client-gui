package ic.swarm.client.guiapp.ui.fs.view


import ic.struct.value.cached.Cached

import ic.gui.scope.ext.views.switch.Switch

import ic.swarm.client.guiapp.ui.common.views.states.ErrorLayout
import ic.swarm.client.guiapp.ui.common.views.states.WaitingLayout
import ic.swarm.client.guiapp.ui.fs.FsWindow
import ic.swarm.client.guiapp.ui.fs.State.*


@Suppress("FunctionName")
internal fun <Content: Any> FsWindow<Content>.ContentLayout() = (

	Switch(
		child = Cached({ state }) {
			when (state) {
				Loading -> {
					WaitingLayout(
						color = accentColor
					)
				}
				ConnectionFailure -> {
					ErrorLayout(
						text = "Can't load :("
					)
				}
				NotSupportedError -> {
					ErrorLayout(
						text = "File not supported :("
					)
				}
				Loaded -> initLoadedContentLayout()
			}
		}
	)

)