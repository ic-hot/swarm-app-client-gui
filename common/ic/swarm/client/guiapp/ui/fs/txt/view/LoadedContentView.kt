package ic.swarm.client.guiapp.ui.fs.txt.view


import ic.graphics.color.Color.Companion.Black

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ext.dp
import ic.gui.font.Font
import ic.gui.scope.ext.views.textinput.TextInput
import ic.gui.view.textinput.Status.*

import ic.swarm.client.guiapp.ui.fs.txt.PlainTextWindow


@Suppress("FunctionName")
internal fun PlainTextWindow.LoadedContentView() = (

	TextInput(
		width = ByContainer, height = ByContainer,
		padding = 8.dp,
		status = if (isContentEditable) Editable else Selectable,
		size = 14.dp,
		font = Font.DefaultMonospace,
		color = Black,
		getValue = { content!! },
		onValueChanged = {
			content = it
			updateView()
		}
	)

)