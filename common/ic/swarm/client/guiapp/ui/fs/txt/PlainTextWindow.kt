package ic.swarm.client.guiapp.ui.fs.txt


import ic.base.strings.ext.toByteSequence
import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.input.ext.read
import ic.stream.output.ByteOutput
import ic.stream.sequence.ext.toString
import ic.util.text.charset.Charset.Companion.Utf8

import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.fs.file.FileWindow
import ic.swarm.client.guiapp.ui.fs.txt.view.LoadedContentView
import ic.swarm.fs.type.ContentType


internal class PlainTextWindow : FileWindow<String>() {


	override val contentType get() = ContentType.File.PlainText

	override val isContentEditable get() = true


	override val accentColor get() = theme.plainTextColor


	override fun initLoadedContentLayout() = LoadedContentView()


	@Throws(IoException::class)
	override fun ByteInput.readContent() : String {
		return read().toString(charset = Utf8)
	}


	@Throws(IoException::class)
	override fun ByteOutput.writeContent (content: String) {
		write(
			content.toByteSequence(charset = Utf8)
		)
	}


}