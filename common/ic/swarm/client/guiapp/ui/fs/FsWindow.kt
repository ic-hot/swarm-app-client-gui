package ic.swarm.client.guiapp.ui.fs


import ic.base.annotations.Blocking
import ic.base.throwables.AccessException
import ic.base.throwables.IoException
import ic.base.throwables.NotSupportedException
import ic.stream.input.ByteInput
import ic.stream.input.ext.readString
import ic.struct.list.List
import ic.stream.output.ByteOutput
import ic.stream.output.ext.writeString
import ic.util.url.Url

import ic.graphics.color.Color

import ic.gui.view.View
import ic.gui.window.Window

import ic.swarm.access.Access.*
import ic.swarm.access.AccessKey
import ic.swarm.access.AccessKey.*
import ic.swarm.access.funs.readNullableAccessKey
import ic.swarm.access.funs.writeNullableAccessKey
import ic.swarm.fs.entry.SwarmFsEntry
import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.link.ext.fromUrl

import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.client.guiapp.model.addr.funs.readFsAddress
import ic.swarm.client.guiapp.model.addr.funs.writeFsAddress
import ic.swarm.client.guiapp.ui.fs.State.*
import ic.swarm.client.guiapp.ui.fs.impl.load
import ic.swarm.client.guiapp.ui.fs.view.FsScreen


internal abstract class FsWindow<Content: Any> : Window() {


	lateinit var titleText : String

	lateinit var address : FsAddress



	abstract val isContentEditable : Boolean

	@Blocking
	@Throws(IoException::class, AccessException::class, NotSupportedException::class)
	abstract fun initEntry() : SwarmFsEntry

	@Blocking
	@Throws(IoException::class, AccessException::class, NotSupportedException::class)
	abstract fun initContent (entry: SwarmFsEntry, ownAccessKey: AccessKey?) : Content

	@Throws(IoException::class)
	final override fun ByteInput.load() {
		titleText = readString()
		address = readFsAddress()
		ownAccessKey = readNullableAccessKey()
	}

	override fun loadFromUrl (url: Url) {
		val link = SwarmLink.fromUrl(url)
		titleText = link.name ?: ""
		address = FsAddress.NotInHomeFolder(
			streamAddress = link.streamAddress,
			access = link.access
		)
		ownAccessKey = when (link.access) {
			Everyone -> None
			is Restricted -> null
		}
	}


	override val isTopDecorLight    get() = true
	override val isBottomDecorLight get() = true

	abstract val accentColor : Color

	abstract fun initEditActionButtons() : List<out View>

	abstract fun initLoadedContentLayout() : View

	final override fun initView() = FsScreen()


	internal var state : State = Loading

	var ownAccessKey : AccessKey? = null

	var entryOrNull : SwarmFsEntry? = null

	var content : Content? = null



	final override fun onOpen() {
		load()
	}


	@Throws(IoException::class)
	final override fun ByteOutput.save() {
		writeString(titleText)
		writeFsAddress(address)
		writeNullableAccessKey(ownAccessKey)
	}


	final override fun onClose() {
		content = null
		entryOrNull = null
		ownAccessKey = null
		state = Loading
	}


}