package ic.swarm.client.guiapp.ui.fs.impl


import ic.swarm.client.guiapp.model.addr.FsAddress.*
import ic.swarm.client.guiapp.ui.fs.FsWindow


internal val FsWindow<*>.isInHomeFolder get() = when (address) {

	is InHomeFolder -> true

	is NotInHomeFolder -> false

}