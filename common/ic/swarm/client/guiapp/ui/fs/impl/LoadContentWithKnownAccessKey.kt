package ic.swarm.client.guiapp.ui.fs.impl


import ic.base.throwables.AccessException
import ic.base.throwables.IoException
import ic.base.throwables.NotSupportedException
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread
import ic.util.log.logD

import ic.swarm.client.guiapp.ui.fs.FsWindow
import ic.swarm.client.guiapp.ui.fs.State


internal fun <Content: Any> FsWindow<Content>.loadContentWithKnownAccessKey() {

	doInBackground {

		try {

			val loadedContent = initContent(
				entry = entryOrNull!!,
				ownAccessKey = ownAccessKey
			)

			doInUiThread {
				content = loadedContent
				state = State.Loaded
				updateView()
			}

		} catch (_: AccessException) {

			logD("FsWindow") { "loadContentWithKnownAccessKey AccessException" }

			doInUiThread {
				askForPassword()
			}

		} catch (_: IoException) {

			doInUiThread {
				state = State.ConnectionFailure
				updateView()
			}

		} catch (_: NotSupportedException) {

			doInUiThread {
				state = State.NotSupportedError
				updateView()
			}

		}

	}

}