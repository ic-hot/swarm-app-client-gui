package ic.swarm.client.guiapp.ui.fs.impl


import ic.swarm.client.guiapp.model.addr.FsAddress.*
import ic.swarm.client.guiapp.ui.fs.FsWindow


private val FsWindow<*>.isParentEditable : Boolean get() {

	return when (address) {

		is InHomeFolder -> true

		is NotInHomeFolder -> false

	}

}


internal val FsWindow<*>.isEditable get() = isParentEditable && isContentEditable