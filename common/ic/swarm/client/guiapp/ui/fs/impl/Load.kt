package ic.swarm.client.guiapp.ui.fs.impl


import ic.base.throwables.AccessException
import ic.base.throwables.IoException
import ic.base.throwables.NotSupportedException
import ic.parallel.annotations.CallbackThread
import ic.design.task.scope.ext.doInBackground
import ic.design.task.scope.ext.doInUiThread

import ic.swarm.client.guiapp.ui.fs.FsWindow
import ic.swarm.client.guiapp.ui.fs.State.*
import ic.util.log.logD


@CallbackThread
internal fun <Content: Any> FsWindow<Content>.load() {

	doInBackground {

		try {

			val loadedEntry = initEntry()

			logD("FsWindow") { "load loadedEntry: $loadedEntry" }

			doInUiThread {
				entryOrNull = loadedEntry
				loadContent()
			}

		} catch (_: IoException) {

			doInUiThread {
				state = ConnectionFailure
				updateView()
			}

		} catch (_: AccessException) {

			throw RuntimeException("AccessException")

		} catch (_: NotSupportedException) {

			throw RuntimeException("NotSupportedException")

		}

	}

}