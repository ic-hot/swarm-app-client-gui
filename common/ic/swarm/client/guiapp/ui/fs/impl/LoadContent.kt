package ic.swarm.client.guiapp.ui.fs.impl


import ic.util.log.logD
import ic.parallel.annotations.CallbackThread

import ic.swarm.access.Access
import ic.swarm.access.AccessKey
import ic.swarm.client.guiapp.ui.fs.FsWindow


@CallbackThread
internal fun <Content: Any> FsWindow<Content>.loadContent() {

	val entry = this.entryOrNull

	val ownAccess = entry?.ownAccess

	logD("FsWindow") { "loadContent ownAccess: $ownAccess" }

	when (ownAccess) {

		null -> {
			ownAccessKey = null
			loadContentWithKnownAccessKey()
		}

		Access.Everyone -> {
			ownAccessKey = AccessKey.None
			loadContentWithKnownAccessKey()
		}

		Access.Restricted.WhoeverHasPassword -> {
			if (ownAccessKey is AccessKey.Private.Password) {
				loadContentWithKnownAccessKey()
			} else {
				askForPassword()
			}
		}

	}

}