package ic.swarm.client.guiapp.ui.fs.impl


import ic.swarm.access.AccessKey
import ic.swarm.client.guiapp.ui.common.popups.enterpassword.showEnterPasswordPopup
import ic.swarm.client.guiapp.ui.fs.FsWindow


internal fun <Content: Any> FsWindow<Content>.askForPassword() {

	showEnterPasswordPopup(
		onCloseWithoutResult = { close() },
		onCloseWithResult = { password ->
			ownAccessKey = ic.swarm.access.AccessKey.Private.Password(password)
			loadContentWithKnownAccessKey()
		}
	)

}