package ic.swarm.client.guiapp.ui.fs


import ic.gui.window.Window

import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.client.guiapp.ui.fs.folder.openFolderWindow
import ic.swarm.client.guiapp.ui.fs.png.openPngImageWindow
import ic.swarm.client.guiapp.ui.fs.txt.openPlainTextWindow
import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.type.ContentType


fun Window.openContentWindow (link: SwarmLink) {

	val address = FsAddress.NotInHomeFolder(
		streamAddress = link.streamAddress,
		access = link.access
	)

	val title = link.name ?: ""

	when (link.contentType) {

		ContentType.Folder -> {
			openFolderWindow(
				title = title,
				address = address
			)
		}

		ContentType.File.PlainText -> {
			openPlainTextWindow(
				title = title,
				address = address
			)
		}

		ContentType.File.PngImage -> {
			openPngImageWindow(
				title = title,
				address = address
			)
		}

		else -> throw NotImplementedError()

	}

}