package ic.swarm.client.guiapp.ui.main.view


import ic.base.throwables.UnableToParseException
import ic.struct.list.List

import ic.gui.control.ext.BottomDecorSpace
import ic.gui.control.ext.TopDecorSpace
import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.view.View

import ic.res.icons.material.MaterialIcon

import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.common.views.buttons.Button
import ic.swarm.client.guiapp.ui.fs.openContentWindow
import ic.swarm.client.guiapp.ui.main.MainWindow
import ic.swarm.client.guiapp.ui.main.impl.openHomeFolderWindow
import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.link.ext.fromUrlOrThrowUnableToParse


@Suppress("FunctionName")
internal fun MainWindow.MainView() : View = (

	Stack(
		width = ByContainer, height = ByContainer,
		children = List(
			Solid(theme.backgroundColor),
			Column(
				width = ByContainer, height = ByContainer,
				children = List(
					TopDecorSpace(),
					TopBar(),
					Space(height = 12.dp),
					ServiceStateBlock(),
					Space(),
					Padding(
						horizontal = 12.dp,
						child = Column(
							height = ByContent,
							children = List(
								Button(
									backgroundColor = theme.folderColor,
									icon = MaterialIcon.ContentPaste,
									text = "Paste link"
								) {
									val string = getStringFromClipboard()
									if (string == null) {
										showToast("No link in the clipboard")
									} else {
										try {
											val link = SwarmLink.fromUrlOrThrowUnableToParse(string)
											openContentWindow(link)
										} catch (_: UnableToParseException) {
											showToast("String in the clipboard is not a link")
										}
									}
								},
								Space(height = 12.dp),
								Button(
									backgroundColor = theme.folderColor,
									icon = MaterialIcon.Folder,
									text = "Home folder"
								) {
									openHomeFolderWindow()
								}
							)
						)
					),
					Space(height = 12.dp),
					BottomDecorSpace()
				)
			)
		)
	)

)