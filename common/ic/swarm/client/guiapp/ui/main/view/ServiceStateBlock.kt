package ic.swarm.client.guiapp.ui.main.view


import ic.struct.list.List
import ic.struct.value.cached.Cached

import ic.graphics.color.Color.Companion.Black
import ic.graphics.color.Color.Companion.EvenLighterGray
import ic.graphics.color.Color.Companion.Gray
import ic.graphics.color.Color.Companion.LightSkyBlue
import ic.graphics.color.Color.Companion.LighterGray

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ByContent
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.column.Column
import ic.gui.scope.ext.views.material.Material
import ic.gui.scope.ext.views.padding.Padding
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.solid.Solid
import ic.gui.scope.ext.views.space.Space
import ic.gui.scope.ext.views.stack.Stack
import ic.gui.scope.ext.views.switch.Switch
import ic.gui.scope.ext.views.text.Text

import ic.swarm.client.guiapp.global.isServiceShouldBeRunning
import ic.swarm.client.guiapp.ui.common.networkCoverageValueText
import ic.swarm.client.guiapp.ui.common.serviceStateMessage
import ic.swarm.client.guiapp.ui.common.views.buttons.Button
import ic.swarm.client.guiapp.ui.main.MainWindow
import ic.swarm.client.guiapp.ui.main.impl.getIndicatorColor
import ic.swarm.client.guiapp.ui.main.impl.startService
import ic.swarm.client.guiapp.ui.main.impl.stopService


@Suppress("FunctionName")
internal fun MainWindow.ServiceStateBlock() = (

	Padding(
		horizontal = 12.dp,
		child = Material(
			outlineColor = LighterGray,
			outlineThickness = 2.dp,
			cornersRadius = 16.dp,
			child = Stack(
				width = ByContainer, height = ByContent,
				children = List(
					Solid(EvenLighterGray),
					Padding(
						padding = 12.dp,
						child = Column(
							width = ByContainer, height = ByContent,
							children = List(
								Row(
									width = ByContainer, height = ByContent,
									children = List(
										Material(
											cornersRadius = 4.dp,
											child = Solid(
												width = 8.dp, height = 8.dp,
												getColor = ::getIndicatorColor
											)
										),
										Space(width = 8.dp),
										Text(
											width = ByContainer,
											size = 18.dp,
											color = Black,
											getValue = { serviceStateMessage }
										)
									)
								),
								Space(height = 8.dp),
								Text(
									width = ByContainer,
									size = 14.dp,
									color = Gray,
									getValue = {
										"Network coverage: $networkCoverageValueText"
									}
								),
								Space(height = 12.dp),
								Switch(
									width = ByContainer, height = ByContent,
									child = Cached({ isServiceShouldBeRunning }) {
										if (isServiceShouldBeRunning) {
											Button(
												text = "Stop node service",
												color = LighterGray,
												onClick = {
													stopService()
												}
											)
										} else {
											Button(
												text = "Start node service",
												color = LightSkyBlue,
												onClick = {
													startService()
												}
											)
										}
									}
								)
							)
						)
					)
				)
			)
		)
	)

)