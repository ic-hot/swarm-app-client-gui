package ic.swarm.client.guiapp.ui.main.view


import ic.struct.list.List
import ic.res.icons.material.MaterialIcon

import ic.gui.dim.dp.ByContainer
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.ext.views.row.Row
import ic.gui.scope.ext.views.space.Space

import ic.swarm.client.guiapp.ui.common.theme.theme
import ic.swarm.client.guiapp.ui.common.views.buttons.FlatIconButton
import ic.swarm.client.guiapp.ui.main.MainWindow
import ic.swarm.client.guiapp.ui.settings.openSettingsWindow


@Suppress("FunctionName")
internal fun MainWindow.TopBar() = (

	Row(
		width = ByContainer, height = 48.dp,
		children = List(

			Space(),

			FlatIconButton(
				icon = MaterialIcon.Cog,
				color = theme.neutralColor,
				onClick = {
					openSettingsWindow()
				}
			)

		)
	)

)