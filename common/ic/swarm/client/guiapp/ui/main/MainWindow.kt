package ic.swarm.client.guiapp.ui.main


import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.input.ext.getNextByte

import ic.gui.window.Window

import ic.swarm.client.guiapp.ui.broadcast.MessageType.ServiceStateChanged
import ic.swarm.client.guiapp.ui.main.view.MainView


internal class MainWindow : Window() {


	override val isCloseable get() = true


	override val title get() = "Swarm explorer"


	override val isTopDecorLight    get() = true
	override val isBottomDecorLight get() = true


	override fun initView() = MainView()


	@Throws(IoException::class)
	override fun ByteInput.readBroadcast() {
		val messageType = getNextByte()
		when (messageType) {
			ServiceStateChanged -> {
				updateView()
			}
		}
	}


}