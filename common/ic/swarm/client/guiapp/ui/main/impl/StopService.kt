package ic.swarm.client.guiapp.ui.main.impl


import ic.gui.broadcast.broadcast

import ic.swarm.client.guiapp.ui.broadcast.MessageType
import ic.swarm.client.guiapp.ui.main.MainWindow


internal fun MainWindow.stopService() {

	broadcast {
		putByte(MessageType.StopService)
	}

}