package ic.swarm.client.guiapp.ui.main.impl


import ic.graphics.color.Color
import ic.graphics.color.Color.Companion.Blue
import ic.graphics.color.Color.Companion.CalmGreen
import ic.graphics.color.Color.Companion.Gray
import ic.graphics.color.Color.Companion.Red

import ic.swarm.status.SwarmStatus.*

import ic.swarm.client.guiapp.global.isServiceShouldBeRunning
import ic.swarm.client.guiapp.global.swarmStatus
import ic.swarm.client.guiapp.ui.main.MainWindow


internal fun MainWindow.getIndicatorColor() : Color {

	if (isServiceShouldBeRunning) {

		return when (swarmStatus) {

			Starting -> Blue

			is Running -> CalmGreen

			Stopping, Idle -> Gray

		}

	} else {

		return Red

	}

}