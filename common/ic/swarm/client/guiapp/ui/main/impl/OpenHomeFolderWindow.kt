package ic.swarm.client.guiapp.ui.main.impl


import ic.struct.list.List

import ic.swarm.client.guiapp.io.fs.getHomeAccessKey
import ic.swarm.client.guiapp.io.fs.isHomeFolderSet
import ic.swarm.client.guiapp.model.addr.FsAddress
import ic.swarm.client.guiapp.ui.fs.folder.openFolderWindow
import ic.swarm.client.guiapp.ui.main.MainWindow
import ic.swarm.client.guiapp.ui.windows.WindowType

import ic.gui.window.scope.ext.openWindow


internal fun MainWindow.openHomeFolderWindow() {

	if (isHomeFolderSet) {

		openFolderWindow(
			title = "Home folder",
			address = FsAddress.InHomeFolder(
				path = List()
			),
			ownAccessKey = getHomeAccessKey()
		)

	} else {

		openWindow {
			putByte(WindowType.InitHomeSwarmDirectory)
		}

	}

}