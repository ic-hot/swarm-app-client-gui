package ic.swarm.client.guiapp.ui.main.impl


import ic.gui.permissions.Permission.Notifications
import ic.gui.window.scope.ext.requestPermission

import ic.swarm.client.guiapp.ui.main.MainWindow
import ic.swarm.client.guiapp.ui.service.openServiceWindow


internal fun MainWindow.startService() {

	requestPermission(Notifications)

	openServiceWindow()

}